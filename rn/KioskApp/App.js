import React from 'react';
import {NativeBaseProvider, Text, Box, extendTheme} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import RootScreen from './app/modules/root-screen/rootScreen.js';

import {mainColor, pxToDp} from './app/common/commonIndex.js';

const theme = extendTheme({
  colors: {
    // Add new color
    theme: {
      main: mainColor,
      50: mainColor + '0D',
      100: mainColor + '1A',
      200: mainColor + '33',
      300: mainColor + '4D',
      400: mainColor + '66',
      500: mainColor,
      600: mainColor,
      700: mainColor + 'B3',
      800: mainColor + 'CC',
      900: mainColor + 'E6',
    },
    whiteBtn: {
      50: '#fff',
      100: '#fff',
      200: '#e7e5e4',
      300: '#fff',
      400: '#fff',
      500: '#fff',
      600: '#fff',
      700: '#e7e5e4',
      800: '#fff',
      900: '#fff',
    },
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: 'light',
  },
  components: {
    Input: {
      baseStyle: {
        height: '50px',
      },
      defaultProps: {},
    },
    Checkbox: {
      baseStyle: {fontSize: pxToDp(14), color: mainColor},
      defaultProps: {
        size: 'md',
        colorScheme: 'theme',
        _text: {
          fontSize: pxToDp(14),
          color: '#333333',
        },
      },
    },
    Button: {
      baseStyle: {},
      defaultProps: {
        colorScheme: 'theme',
      },
    },
    IconButton: {
      baseStyle: {},
      defaultProps: {
        colorScheme: 'theme',
      },
    },
    Avatar: {
      baseStyle: {backgroundColor: '#CCCACA'},
      defaultProps: {},
    },
    Menu: {
      baseStyle: {p: '0px'},
      defaultProps: {},
    },
    MenuItem: {
      baseStyle: {
        _text: {fontSize: '16px', color: '#7A7979', lineHeight: '36px'},
        minW: '100px',
        justifyContent: 'center',
        h: '36px',
        px: '9px',
        py: '0px',
      },
    },
  },
});

function HomeScreen() {
  return (
    <Box style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
    </Box>
  );
}

function SettingsScreen() {
  return (
    <Box style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Settings!</Text>
    </Box>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NativeBaseProvider theme={theme}>
      {/*  <Tab.Navigator>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Settings" component={SettingsScreen} />
        </Tab.Navigator>*/}
      <RootScreen />
    </NativeBaseProvider>
  );
}

// export default function App() {
//   return (
//     <NativeBaseProvider>
//       <NavigationContainer>
//         <Box flex={1} bg="#fff" alignItems="center" justifyContent="center">
//           <Text>Open up App.js to start working on your app!</Text>
//         </Box>
//       </NavigationContainer>
//     </NativeBaseProvider>
//   );
// }

// import React from 'react';
// import type {Node} from 'react';
// import {
//   SafeAreaView,
//   ScrollView,
//   StatusBar,
//   StyleSheet,
//   Text,
//   useColorScheme,
//   View,
// } from 'react-native';

// import {
//   Colors,
//   DebugInstructions,
//   Header,
//   LearnMoreLinks,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

// /* $FlowFixMe[missing-local-annot] The type annotation(s) required by Flow's
//  * LTI update could not be added via codemod */
// const Section = ({children, title}): Node => {
//   const isDarkMode = useColorScheme() === 'dark';
//   return (
//     <View style={styles.sectionContainer}>
//       <Text
//         style={[
//           styles.sectionTitle,
//           {
//             color: isDarkMode ? Colors.white : Colors.black,
//           },
//         ]}>
//         {title}
//       </Text>
//       <Text
//         style={[
//           styles.sectionDescription,
//           {
//             color: isDarkMode ? Colors.light : Colors.dark,
//           },
//         ]}>
//         {children}
//       </Text>
//     </View>
//   );
// };

// const App: () => Node = () => {
//   const isDarkMode = useColorScheme() === 'dark';

//   const backgroundStyle = {
//     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
//   };

//   return (
//     <SafeAreaView style={backgroundStyle}>
//       <StatusBar
//         barStyle={isDarkMode ? 'light-content' : 'dark-content'}
//         backgroundColor={backgroundStyle.backgroundColor}
//       />
//       <ScrollView
//         contentInsetAdjustmentBehavior="automatic"
//         style={backgroundStyle}>
//         <Header />
//         <View
//           style={{
//             backgroundColor: isDarkMode ? Colors.black : Colors.white,
//           }}>
//           <Section title="Step One">
//             Edit <Text style={styles.highlight}>App.js</Text> to change this
//             screen and then come back to see your edits.
//           </Section>
//           <Section title="See Your Changes">
//             <ReloadInstructions />
//           </Section>
//           <Section title="Debug">
//             <DebugInstructions />
//           </Section>
//           <Section title="Learn More">
//             Read the docs to discover what to do next:
//           </Section>
//           <LearnMoreLinks />
//         </View>
//       </ScrollView>
//     </SafeAreaView>
//   );
// };

// const styles = StyleSheet.create({
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//   },
//   highlight: {
//     fontWeight: '700',
//   },
// });

// export default App;
