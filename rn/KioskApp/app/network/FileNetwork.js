import {
	post,
	get,
	uploadImg,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	baseImgUrl,
	getResponseError,
} from './BaseNetwork.js';

/*活动列表*/
export function fileUpload(img) {
	let formData = new FormData();
	let file = {
		uri: img.path,
		type: img.mime,
		name:
			Math.round(Math.random() * 10000000).toString() +
			(img.filename ?? 'APP.jpg'),
	};
	formData.append('file', file);
	console.log(img);
	console.log(file);
	console.log(formData);
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return uploadImg(baseImgUrl + 'api/file/fileUpload', true, formData)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
