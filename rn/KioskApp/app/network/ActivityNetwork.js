import {
	post,
	get,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	getResponseError,
} from './BaseNetwork.js';

/*活动列表*/
export function findList(page = 1, keyword = null, size = 10) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/promotion/findList',
			{page: page, keyword: keyword, size: size},
			true,
			false,
		)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data?.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*详情*/
export function detail(id) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return get(baseUrl + 'app/promotion/detail', {id: id}, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取未读活动数*/
export function getMessageNum() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/promotion/findPromotionNum', {}, true, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
