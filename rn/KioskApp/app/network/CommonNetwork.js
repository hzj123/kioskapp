import {
	post,
	get,
	uploadImg,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	baseImgUrl,
	getResponseError,
} from './BaseNetwork.js';

/*留言板新增*/
export function addMsgBoard(params) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/promotionMessage/add', params, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
