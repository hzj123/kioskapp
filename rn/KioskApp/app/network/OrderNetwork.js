import {
	post,
	get,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	getResponseError,
} from './BaseNetwork.js';

/*工单列表*/
export function findByParam(page = 1, keyword = null, size = 10) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return get(
			baseUrl + 'app/ticket/findByParam',
			{page: page, keyword: keyword, size: size},
			true,
		)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data?.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*工单详情*/
export function detail(id) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return get(baseUrl + 'app/ticket/detail', {id: id}, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*对话列表*/
export function queryComm(ticketId) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/ticket/queryComm',
			{ticketId: ticketId},
			true,
			false,
		)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*工单类别*/
export function orderTypeList() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/ticketType/list', {}, true, false)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*新增工单*/
export function add(params) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/ticket/add', params, true, true)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*发送对话*/
export function addComm(params) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/ticket/addComm', params, true, true)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取未读工单数*/
export function getMessageNum() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/ticket/getMessageNum', {}, true, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
