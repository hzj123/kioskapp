import {Promise as ZJPromise} from 'bluebird';

ZJPromise.config({
	// Enable warnings
	warnings: true,
	// Enable long stack traces
	longStackTraces: true,
	// Enable cancellation
	cancellation: true,
	// Enable monitoring
	monitoring: false,
	// Enable async hooks
	asyncHooks: false,
});

export {ZJPromise};

export const ErrorType = {
	need_login: 'need_login',
	network: 'please try later',
	timeout: 'The request timeout',
};

export class ZJError extends Error {
	constructor(msg, code = 500) {
		super(msg, '', '');
		this.code = code;
	}
}

export const onCancelError = new ZJError('onCancel');

/*基础路径*/
export const baseUrl = 'http://3.101.56.185:8870/';
// export const baseUrl = 'http://192.168.0.141:8870/';

export const baseImgUrl = 'http://3.101.56.185:8870/';

/*获取头部信息*/
export function getHeader(haveToken = true, other = {}) {
	return new Promise((resolve, reject) => {
		var header = {
			accept: 'application/json',
			'Content-Type': 'application/json-patch+json',
		};
		if (other) {
			for (var obj in other) {
				header[obj] = other[obj];
			}
		}
		if (haveToken === true) {
			if (global.token != null) {
				header.token = global.token;
			} else {
				reject(getResponseError(ErrorType.need_login));
			}
		}
		resolve(header);
	});
}

/*获取头部信息*/
export function getResponseError(msg = '', code = 400) {
	return new ZJError(msg, code);
}

/**
 * GET 请求时，拼接请求URL
 * @param url 请求URL
 * @param params 请求参数
 * @returns {*}
 */
const handleUrl = url => params => {
	if (params) {
		let paramsArray = [];
		Object.keys(params).forEach(key => {
			if (params[key] != null) {
				paramsArray.push(key + '=' + encodeURIComponent(params[key]));
			}
		});
		if (url.search(/\?/) === -1) {
			typeof params === 'object' ? (url += '?' + paramsArray.join('&')) : url;
		} else {
			url += '&' + paramsArray.join('&');
		}
	}
	return url;
};

/**
 * 生成fetch
 * @param {*} url 地址
 * @param {*} header header
 * @param {*} method 请求类型
 * @param {*} paramInBody 请求参数是否在body中
 * @param {*} params 请求参数
 * @param {*} toJson 当参数在body中时，是否转换为json
 * @param {*} timeout 请求超时的时间
 */
function _getFetch(
	url,
	method,
	header,
	params = {},
	paramInBody,
	toJson = true,
	timeout = 30000,
) {
	var url = handleUrl(url)(paramInBody ? undefined : params);
	var body = paramInBody
		? toJson
			? JSON.stringify(params)
			: params
		: undefined;
	console.log('-------request start------');
	console.log(url);
	console.log(method);
	console.log(header);
	console.log(body);
	console.log('-------request end------');
	let fetchMethod = fetch(url, {
		method: method,
		headers: header,
		body: body,
	});

	//---超时设置---
	let timeoutBlock = () => {};
	let timeout_promise = new Promise((resolve, reject) => {
		timeoutBlock = () => {
			// 请求超时处理
			// reject(ErrorType.timeout);
			reject(getResponseError(ErrorType.timeout));
		};
	});

	// Promise.race(iterable)方法返回一个promise
	// 这个promise在iterable中的任意一个promise被解决或拒绝后，立刻以相同的解决值被解决或以相同的拒绝原因被拒绝。
	let abortable_promise = Promise.race([fetchMethod, timeout_promise]);

	setTimeout(() => {
		timeoutBlock();
	}, timeout);

	return abortable_promise;
}

export function post(
	url,
	params = {},
	haveToken = true,
	paramInBody = true,
	toJson = true,
) {
	return new Promise((resolve, reject) => {
		getHeader(haveToken)
			.then(header =>
				_getFetch(url, 'POST', header, params, paramInBody, toJson),
			)
			.then(response => response.json())
			.then(response => {
				console.log(response);
				resolve(response);
			})
			.catch(err => {
				console.log(err);
				reject(err);
			});
	});
}

export function get(url, params = {}, haveToken = true) {
	return new Promise((resolve, reject) => {
		getHeader(haveToken)
			.then(header => _getFetch(url, 'GET', header, params, false, false))
			.then(response => response.json())
			.then(response => {
				console.log(response);
				resolve(response);
			})
			.catch(err => {
				console.log(err);
				reject(err);
			});
	});
}

/**
 * 上传图片
 * @param url 请求的URL
 * @param haveToken 是否包含token
 * @param params 请求参数
 * @returns {Promise}
 */
export function uploadImg(url, haveToken = true, params = {}) {
	let promise = new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return getHeader(haveToken, {
			'Content-Type': 'multipart/form-data;charset=utf-8', //'multipart/form-data',
		})
			.then(header => _getFetch(url, 'POST', header, params, true, false))
			.then(response => response.json())
			.then(response => {
				console.log(response);
				resolve(response);
			})
			.catch(err => {
				console.log(err);
				reject(err);
			});
	});
	return promise;
}
