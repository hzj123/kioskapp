import {
	post,
	get,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	getResponseError,
} from './BaseNetwork.js';

/*获取用户信息*/
export function findUserId() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/findUserId', {}, true, false)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*个人资料编辑*/
export function updateUser(info = {}) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/updateUser', info, true, true)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*修改密码*/
export function changePassword(oldPassword, newPassword) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/user/changePassword',
			{newPassword: newPassword, oldPassword: oldPassword},
			true,
			false,
		)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*联系客服*/
export function customerService() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/customerService', {}, true, true)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*用户员工列表*/
export function list(page = 1, size = 10) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return get(baseUrl + 'app/user/list', {page: page, size: size}, true)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data?.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*员工资料-获取所有店铺*/
export function getAllShop() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/getAllShop', {}, true, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*员工资料-新增/编辑员工*/
export function saveCustomerUserExtend(params = {}) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/saveCustomerUserExtend', params, true, true)
			.then(response => {
				if (response.code === 200) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取常见问题类型*/
export function getQuestionType() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/getQuestionType', {}, true, true)
			.then(response => {
				if (response.code === 200) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取常见问题*/
export function getCommonQuestion(
	questionTypeId, //常见问题类型id
	keyword,
	page = 1,
	size = 10,
) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/user/getCommonQuestion',
			{
				questionTypeId: questionTypeId,
				keyword: keyword,
				page: page,
				size: size,
			},
			true,
			false,
		)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取分页消息列表*/
export function getPageMessages(
	type = 0, //0:工单消息，1:系统消息
	page = 1,
	size = 10,
) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/user/getPageMessages',
			{
				type: type,
				page: page,
				size: size,
			},
			true,
			false,
		)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data?.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*获取未读消息数量*/
export function getMessagesCount() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/getMessagesCount', {}, true, false)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
					// resolve({messagesCount: 1});
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*更新员工语音类型 1-中文，20英文*/
export function updateLanguage(type) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/updateLanguage', {type: type}, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*删除员工*/
export function deleteEmployee(id) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/user/delete', {id: id}, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*app注册*/
export function register(username, password) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'user/register',
			{username: username, password: password},
			false,
			false,
		)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*app注销账号*/
export function logOff() {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'user/appLogOff', {}, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve();
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
