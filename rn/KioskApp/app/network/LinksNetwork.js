import {
	post,
	get,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	getResponseError,
} from './BaseNetwork.js';

/*活动列表*/
export function queryByParam(page = 1, size = 10) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/usedLinks/queryByParam',
			{page: page, size: size},
			true,
			true,
		)
			.then(response => {
				if (response.code === 200 && response.data?.list != null) {
					resolve(response.data?.list);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*详情*/
export function detail(id) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return get(baseUrl + 'app/usedLinks/detail', {id: id}, true)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*新增用户链接*/
export function add(info = {}) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/usedLinks/add', info, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*删除用户链接*/
export function deleteItem(id) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/usedLinks/delete', {id: id}, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}

/*修改用户链接*/
export function update(info = {}) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(baseUrl + 'app/usedLinks/update', info, true, false)
			.then(response => {
				if (response.code === 200) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
