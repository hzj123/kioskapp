import {
	post,
	onCancelError,
	ZJError,
	ErrorType,
	ZJPromise,
	baseUrl,
	getResponseError,
} from './BaseNetwork.js';

export function login(username, password) {
	return new ZJPromise((resolve, reject, onCancel) => {
		onCancel(() => {
			reject(onCancelError);
		});
		return post(
			baseUrl + 'app/login/assessToken',
			{
				username: username,
				password: password,
			},
			false,
			false,
		)
			.then(response => {
				if (response.code === 200 && response.data != null) {
					resolve(response.data);
				} else {
					reject(getResponseError(response.message, response.code));
				}
			})
			.catch(err => {
				reject(err);
			});
	});
}
