/*检查图片类型*/
export function checkImageType(image) {
	if (!image) {
		return false;
	}
	if (image?.mime !== 'image/png' && image?.mime !== 'image/jpeg') {
		return false;
	}
	return true;
}
