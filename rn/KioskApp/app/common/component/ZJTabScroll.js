import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	ScrollView,
	Pressable,
	Input,
} from 'native-base';

import {mainColor, pxToDp} from '../commonIndex.js';

import PagerView from 'react-native-pager-view';

export default function ZJTabScroll(props) {
	const [data, setData] = React.useState([]);

	const [itemMinW, setItemMinW] = React.useState(props.itemMinW);

	React.useEffect(() => {
		setItemMinW(props.itemMinW);
	}, [props.itemMinW]);

	React.useEffect(() => {
		setData(props.data ?? []);
	}, [props.data]);

	const _itemWidth = props.itemWidth ?? pxToDp(375);

	/*头部分类的item*/
	const _returnTypeItem = ({item, index}) => {
		return (
			<Pressable onPress={() => _onClickTypeItem(index)}>
				<Box
					h="100%"
					minW={itemMinW ?? pxToDp(60)}
					px={pxToDp(8)}
					position="relative"
					alignItems="center"
					justifyContent="center">
					<Text color={index === selectIndex ? mainColor : '#333'}>
						{item.title}
					</Text>
					{index === selectIndex ? (
						<Box
							bg={mainColor}
							w={pxToDp(24)}
							h={pxToDp(2)}
							position="absolute"
							bottom={0}
						/>
					) : null}
				</Box>
			</Pressable>
		);
	};

	/*底部分类列表*/
	const _returnListItem = ({item, index}) => {
		return <Box w={_itemWidth}>{item.page ? item.page : null}</Box>;
	};

	/*分类选中项*/
	const [selectIndex, setSelectIndex] = React.useState(0);

	const _topListRef = React.useRef(null);
	const _listRef = React.useRef(null);

	/*点击分类*/
	const _onClickTypeItem = index => {
		if ((selectIndex != null, selectIndex != index && index < data.length)) {
			setSelectIndex(index);
			_listRef?.current?.setPage(index);
		}
	};

	/*去选中分类（不触发底部滚动）*/
	const _topListscrollTo = index => {
		if ((selectIndex != null, selectIndex != index && index < data.length)) {
			setSelectIndex(index);
			_topListRef?.current?.scrollToIndex({
				animated: true,
				index: index,
			});
		}
	};

	return (
		<Box alignItems="center" {...props}>
			<Box
				maxW={'100%'}
				w={props.isLeft ? '100%' : null}
				h={pxToDp(40)}
				px={pxToDp(16)}
				my={pxToDp(4)}>
				<FlatList
					ref={_topListRef}
					showsHorizontalScrollIndicator={false}
					keyboardDismissMode="on-drag"
					horizontal={true}
					data={data}
					renderItem={_returnTypeItem}
					keyExtractor={(item, index) => item + index}
				/>
			</Box>
			<PagerView
				ref={_listRef}
				style={{flex: 1, width: '100%'}}
				onPageSelected={event => {
					var position = event?.nativeEvent?.position;
					if (position != null) {
						_topListscrollTo(position);
					}
				}}
				initialPage={0}>
				{/*<BottomList />*/}
				{data.map((item, index) => {
					return item.page ? (
						<item.page
							key={item + index}
							selected={index == selectIndex}
							keyWord={props.keyWord}
							toRefresh={props.toRefresh}
						/>
					) : (
						<Box key={item + index} />
					);
				})}
			</PagerView>
		</Box>
	);
}
