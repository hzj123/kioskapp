import React from 'react';

import {Box} from 'native-base';
import Spinner from 'react-native-spinkit';
import {mainColor} from '../config.js';

export default function Loading(argument) {
	// const s = [
	// 	'CircleFlip',
	// 	'Bounce',
	// 	'Wave',
	// 	'WanderingCubes',
	// 	'Pulse',
	// 	'ChasingDots',
	// 	'ThreeBounce',
	// 	'Circle',
	// 	'9CubeGrid',
	// 	'WordPress',
	// 	'FadingCircle',
	// 	'FadingCircleAlt',
	// 	'Arc',
	// 	'ArcAlt',
	// 	'Plane',
	// ];

	// var type = s[Math.floor(Math.random() * s.length)];
	return (
		<Box
			position="absolute"
			zIndex="100"
			w="100%"
			h="100%"
			// bg="#0000001A"
			justifyContent="center"
			alignItems="center">
			<Spinner
				isVisible={true}
				size={66}
				type={'FadingCircleAlt'}
				color={mainColor}
			/>
		</Box>
	);
}
