import React from 'react';

import {Pressable, Button, Modal, VStack} from 'native-base';
import {mainColor} from '../config.js';
import pxToDp from '../reactDom.js';

import {useTranslation} from 'react-i18next';

import ImagePicker from 'react-native-image-crop-picker';

/*检查图片类型*/
function checkImageType(image) {
	if (!image) {
		return false;
	}
	if (image?.mime !== 'image/png' && image?.mime !== 'image/jpeg') {
		return false;
	}
	return true;
}

/*底部选择图片*/
export default function ZJChoosePhoto(props) {
	const {t} = useTranslation();

	const [openPhotoModal, setOpenPhotoModal] = React.useState(false);

	const onClickItem = idx => {
		setOpenPhotoModal(false);
		if (idx === 0) {
			ImagePicker.openPicker({
				width: 300,
				height: 300,
				cropping: true,
				mediaType: 'photo',
			})
				.then(image => {
					// console.log(image);
					if (checkImageType(image)) {
						props.getImage ? props.getImage(image) : null;
					} else {
						global.showToast({title: 'Wrong image format'});
					}
				})
				.catch(err => {
					console.log(err);
					global.showToast({title: err?.message});
				});
		} else if (idx === 1) {
			ImagePicker.openCamera({
				width: 300,
				height: 300,
				cropping: true,
				mediaType: 'photo',
			})
				.then(image => {
					// console.log(image);
					if (checkImageType(image)) {
						props.getImage ? props.getImage(image) : null;
					} else {
						global.showToast({title: 'Wrong image format'});
					}
				})
				.catch(err => {
					console.log(err);
					global.showToast({title: err?.message});
				});
		}
	};
	return (
		<Pressable {...props} onPress={() => setOpenPhotoModal(true)}>
			{props.children}
			<Modal
				size="full"
				isOpen={openPhotoModal}
				onClose={() => setOpenPhotoModal(false)}>
				<Modal.Content mt="auto">
					<Modal.Body>
						<VStack>
							<Button
								variant="unstyled"
								h={pxToDp(54)}
								onPress={() => onClickItem(0)}>
								{t('common.img_photo')}
							</Button>
							<Button
								variant="unstyled"
								h={pxToDp(54)}
								onPress={() => onClickItem(1)}>
								{t('common.img_camera')}
							</Button>
						</VStack>
					</Modal.Body>
				</Modal.Content>
			</Modal>
		</Pressable>
	);
}
