import React from 'react';

import {Pressable, Button, Modal, TextArea, Box} from 'native-base';
import {mainColor} from '../config.js';
import pxToDp from '../reactDom.js';

import {useTranslation} from 'react-i18next';
import {ThemeButton} from './ZJComponent.js';

import * as CommonNetwork from '../../network/CommonNetwork.js';

/*
 * 留言弹窗
 * promotionId - 活动id
 */
export default function ZJLeaveMsgModal(props) {
	const {t} = useTranslation();
	const [openPhotoModal, setOpenPhotoModal] = React.useState(false);
	const [content, setContent] = React.useState('');
	const _textAreaRef = React.useRef(null);

	const onClickItem = idx => {
		if (1 === idx) {
			global.showLoading();
			CommonNetwork.addMsgBoard({
				message: content,
				promotionId: props.promotionId,
			})
				.then(res => {
					global.hiddenLoading();
					setContent('');
					setOpenPhotoModal(false);
					_clearTextArea();
				})
				.catch(err => {
					global.hiddenLoading();
					global.showToast({title: err.message});
				});
		} else {
			setOpenPhotoModal(false);
			_clearTextArea();
		}
	};

	const _clearTextArea = () => {
		setContent('');
		_textAreaRef?.current?.blur();
		setTimeout(() => {
			_textAreaRef?.current?.clear();
		}, 250);
	};

	return (
		<Pressable {...props} onPress={() => setOpenPhotoModal(true)}>
			{props.children}
			<Modal
				{...props}
				avoidKeyboard={true}
				size="lg"
				isOpen={openPhotoModal}
				onClose={() => setOpenPhotoModal(false)}>
				<Modal.Content borderRadius={pxToDp(14)} px={pxToDp(16)}>
					<Modal.Header
						alignItems="center"
						borderBottomWidth={0}
						_text={{
							fontSize: pxToDp(16),
							fontWeight: 500,
							color: '#262626',
						}}>
						{t('common.leave_msg_board')}
					</Modal.Header>
					<Modal.Body px={0} pt={0}>
						<Box h={pxToDp(124)} bg="#eee" borderRadius={pxToDp(8)}>
							<TextArea
								ref={_textAreaRef}
								flex={1}
								maxLength={100}
								variant="unstyled"
								placeholderTextColor="#999"
								placeholder={t('placeholder.msg_board')}
								fontSize={pxToDp(14)}
								// value={content}
								// defaultValue={defaultValue}
								onChangeText={e => setContent(e)}
							/>
						</Box>
					</Modal.Body>
					<Modal.Footer p={0} pb={pxToDp(20)} borderTopWidth={0}>
						<Button.Group space={2} flex={1}>
							<ThemeButton
								flex={1}
								mr={14.1}
								onPress={() => onClickItem(1)}
								isDisabled={!(content?.length > 0 && props.promotionId > 0)}
								title={t('common.ok')}
							/>
							<ThemeButton
								flex={1}
								colorScheme="whiteBtn"
								color="#7A7979"
								onPress={() => onClickItem(0)}
								title={t('common.cancel')}
							/>
						</Button.Group>
					</Modal.Footer>
				</Modal.Content>
			</Modal>
		</Pressable>
	);
}
