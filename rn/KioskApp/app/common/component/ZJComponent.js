import React from 'react';

import {Box, Text, Pressable, Button, Modal, VStack} from 'native-base';
import {mainColor} from '../config.js';
import * as JFLIcon from './JFLIcon.js';
import pxToDp from '../reactDom.js';

import {useTranslation} from 'react-i18next';
import {RefreshControl, Dimensions} from 'react-native';

/*主题按钮*/
export function ThemeButton(props) {
	return (
		<Button borderRadius={pxToDp(4)} {...props}>
			{props.title ? (
				<Text
					fontSize={props.fontSize ?? pxToDp(14)}
					fontWeight={props.fontWeight ?? '500'}
					bold={props.bold}
					color={props.color ?? 'white'}>
					{props.title}
				</Text>
			) : null}
			{props.children}
		</Button>
	);
}

/*自定义选择器*/
export function ZJCheckBox(argument) {
	return (
		<Pressable onPress={argument.onPress}>
			<Box flexDir="row" mb={pxToDp(20)}>
				{argument.hiddenIcon ? null : (
					<Box size={pxToDp(20)} mr={pxToDp(10)} mt={pxToDp(2)}>
						{argument.IsChecked ? (
							<JFLIcon.icon_isChecked />
						) : (
							<JFLIcon.icon_isUnChecked />
						)}
					</Box>
				)}
				<Text flex={1} fontSize={pxToDp(14)} color="#333">
					{argument.title}
				</Text>
			</Box>
		</Pressable>
	);
}

/*选择弹窗*/
export function InfoModal(props) {
	const {t} = useTranslation();
	return (
		<Modal avoidKeyboard={true} size="lg" {...props}>
			<Modal.Content borderRadius={pxToDp(14)} px={pxToDp(16)}>
				{props.title ? (
					<Modal.Header
						_text={{fontSize: pxToDp(19), fontWeight: 500, color: '#262626'}}>
						{props.title}
					</Modal.Header>
				) : null}
				{props.info ? (
					<Modal.Body py={props.title?.length > 0 ? pxToDp(16) : pxToDp(30)}>
						<Text textAlign="center" fontSize={pxToDp(19)}>
							{props.info}
						</Text>
					</Modal.Body>
				) : null}
				<Modal.Footer p={0} pb={pxToDp(20)} borderTopWidth={0}>
					<Button.Group space={2} flex={1}>
						<ThemeButton
							flex={1}
							mr={14.1}
							onPress={props.clickOKBtn}
							title={t('common.ok')}
						/>
						<ThemeButton
							flex={1}
							colorScheme="whiteBtn"
							color="#7A7979"
							onPress={props.onClose}
							title={t('common.cancel')}
						/>
					</Button.Group>
				</Modal.Footer>
			</Modal.Content>
		</Modal>
	);
}

/*底部选择列表*/
export function BottomChooseList(props) {
	const {t} = useTranslation();

	const [selectItem, setSelectItem] = React.useState(null);
	React.useEffect(() => {
		setSelectItem(props.selectItem);
	}, [props.selectItem]);

	return (
		<Modal {...props} size="full">
			<Modal.Content mt="auto">
				<Modal.Header
					flexDir="row"
					py="0"
					h={pxToDp(54)}
					_text={{fontSize: pxToDp(18), fontWeight: 500, color: '#262626'}}
					justifyContent="space-between">
					<Button
						onPress={props.onClose}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: '#333'}}>
						{t('common.cancel')}
					</Button>
					<Button
						onPress={
							props.clickOKBtn ? () => props.clickOKBtn(selectItem) : undefined
						}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: mainColor}}>
						{t('common.save')}
					</Button>
				</Modal.Header>
				<Modal.Body pt={0} px={0}>
					<VStack>
						{props.list
							? props.list.map((item, index) => {
									return (
										<Pressable
											key={item + index}
											onPress={() => setSelectItem(item)}>
											<Box
												borderBottomWidth={'1px'}
												borderBottomColor="#E5E6EB"
												alignItems="center"
												justifyContent="center"
												h={pxToDp(54)}>
												<Text
													color={item == selectItem ? mainColor : '#1D2129'}
													textAlign="center"
													fontSize={pxToDp(16)}>
													{item}
												</Text>
											</Box>
										</Pressable>
									);
							  })
							: null}
					</VStack>
				</Modal.Body>
				{props.children}
			</Modal.Content>
		</Modal>
	);
}

/*底部选择图片*/
export function BottomChoosePhoto(props) {
	return (
		<Modal {...props} size="full">
			<Modal.Content mt="auto">
				<Modal.Body>
					<VStack>
						<Button
							variant="unstyled"
							h={pxToDp(54)}
							onPress={props.onClickItem ? () => props.onClickItem(0) : null}>
							相册
						</Button>
						<Button
							variant="unstyled"
							h={pxToDp(54)}
							onPress={props.onClickItem ? () => props.onClickItem(1) : null}>
							相机
						</Button>
					</VStack>
				</Modal.Body>
				{props.children}
			</Modal.Content>
		</Modal>
	);
}

/*空页面*/
export function EmptyView(props) {
	const {t} = useTranslation();
	return (
		<Box
			w="100%"
			pt="50%"
			alignItems="center"
			justifyContent="center"
			{...props}>
			<JFLIcon.emptyView_icon {...props._icon} bg="#999" />
			<Text fontSize={pxToDp(14)} color="#999" {...props._text}>
				{props.title ?? t('common.empty_no_data')}
			</Text>
		</Box>
	);
}

/*列表加载组件-头部*/
export function ListRefresh(props) {
	const {t} = useTranslation();

	return (
		<RefreshControl
			title={t('component.refresh_tip')}
			tintColor={mainColor}
			colors={[mainColor]}
			{...props}
		/>
	);
}

/*列表加载组件-底部*/
export function ListLoadMore(props) {
	const {t} = useTranslation();

	return (
		<Box py={pxToDp(8)} {...props}>
			{props.show ? (
				<Text w="100%" textAlign="center" color="#999" fontSize={pxToDp(14)}>
					{props.nomore
						? props.nomoreTip ?? t('component.loadmore_nomoreTip')
						: props.loading
						? t('component.loadmore_loading')
						: t('component.loadmore_loadmore')}
				</Text>
			) : null}
		</Box>
	);
}

import FastImage from 'react-native-fast-image';
import {baseImgUrl} from '../../network/BaseNetwork.js';
import ImageZoom from 'react-native-image-pan-zoom';
import {useHeaderHeight} from '@react-navigation/elements';

/*有缓存功能的图片*/
export function ZJImage(props) {
	const headerHeight = useHeaderHeight();

	const [uri, setUri] = React.useState('');
	React.useEffect(() => {
		if (props.uri != null) {
			if (
				props.uri.substr(0, 4) == 'http' ||
				props.uri.substr(0, 4) == 'file'
			) {
				setUri(props.uri);
			} else {
				setUri(baseImgUrl + props.uri);
			}
		} else {
			setUri('');
		}
	}, [props.uri]);

	const [openModal, setOpenModal] = React.useState(false);

	return (
		<Pressable
			overflow="hidden"
			bg="#eee"
			onPress={() => setOpenModal(true)}
			{...props}>
			<FastImage
				style={{flex: 1, 'background-color': '#000'}}
				source={{
					uri: uri ?? '',
					priority: FastImage.priority.normal,
				}}
				resizeMode={FastImage.resizeMode.cover}
			/>
			<Modal size="full" isOpen={openModal} onClose={() => setOpenModal(false)}>
				<>
					<Box flex={1} bg="#000">
						<ImageZoom
							cropWidth={Dimensions.get('window').width}
							cropHeight={Dimensions.get('window').height}
							imageWidth={Dimensions.get('window').width}
							imageHeight={Dimensions.get('window').height}>
							<FastImage
								style={{flex: 1, 'background-color': '#000'}}
								source={{
									uri: uri ?? '',
									priority: FastImage.priority.normal,
								}}
								resizeMode={FastImage.resizeMode.contain}
							/>
						</ImageZoom>
					</Box>
					<Pressable
						position="absolute"
						left={10 + 'px'}
						top={(headerHeight > 44 ? headerHeight - 44 : 20) + 'px'}
						size={44 + 'px'}
						alignItems="center"
						justifyContent="center"
						onPress={() => {
							console.log(headerHeight);
							setOpenModal(false);
						}}>
						<JFLIcon.IconClose />
					</Pressable>
				</>
			</Modal>
		</Pressable>
	);
}

/*选择订单类别列表*/
export function ChooseOrderTypeList(props) {
	const {t, i18n} = useTranslation();

	const [selectItem, setSelectItem] = React.useState(null);
	React.useEffect(() => {
		setSelectItem(props.selectItem);
	}, [props.selectItem]);

	return (
		<Modal {...props} size="full">
			<Modal.Content mt="auto">
				<Modal.Header
					flexDir="row"
					py="0"
					h={pxToDp(54)}
					_text={{fontSize: pxToDp(18), fontWeight: 500, color: '#262626'}}
					justifyContent="space-between">
					<Button
						onPress={props.onClose}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: '#333'}}>
						{t('common.cancel')}
					</Button>
					<Button
						onPress={
							props.clickOKBtn ? () => props.clickOKBtn(selectItem) : undefined
						}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: mainColor}}>
						{t('common.save')}
					</Button>
				</Modal.Header>
				<Modal.Body pt={0} px={0}>
					<VStack>
						{props.list
							? props.list.map((item, index) => {
									return (
										<Pressable
											key={item + index}
											onPress={() => setSelectItem(item)}>
											<Box
												borderBottomWidth={'1px'}
												borderBottomColor="#E5E6EB"
												alignItems="center"
												justifyContent="center"
												h={pxToDp(54)}>
												<Text
													color={item == selectItem ? mainColor : '#1D2129'}
													textAlign="center"
													fontSize={pxToDp(16)}>
													{(i18n.language == 'zh' ? item.altType : item.type) ??
														''}
												</Text>
											</Box>
										</Pressable>
									);
							  })
							: null}
					</VStack>
				</Modal.Body>
				{props.children}
			</Modal.Content>
		</Modal>
	);
}

/*选择订单商店列表*/
export function ChooseOrderStoreList(props) {
	const {t, i18n} = useTranslation();

	const [selectItems, setSelectItems] = React.useState(null);
	React.useEffect(() => {
		setSelectItems(props.selectItems);
	}, [props.selectItems]);

	const contain = (array, item) => {
		for (var i = array.length - 1; i >= 0; i--) {
			if (array[i].shopId == item.shopId) {
				return i;
			}
		}
		return -1;
	};

	return (
		<Modal {...props} size="full">
			<Modal.Content mt="auto">
				<Modal.Header
					flexDir="row"
					py="0"
					h={pxToDp(54)}
					_text={{fontSize: pxToDp(18), fontWeight: 500, color: '#262626'}}
					justifyContent="space-between">
					<Button
						onPress={props.onClose}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: '#333'}}>
						{t('common.cancel')}
					</Button>
					<Button
						onPress={
							props.clickOKBtn ? () => props.clickOKBtn(selectItems) : undefined
						}
						variant="unstyled"
						_text={{fontSize: pxToDp(16), fontWeight: 400, color: mainColor}}>
						{t('common.save')}
					</Button>
				</Modal.Header>
				<Modal.Body pt={0} px={0}>
					<VStack>
						{props.list
							? props.list.map((item, index) => {
									return (
										<Pressable
											key={item + index}
											onPress={() => {
												var array = [...selectItems];
												var index = contain(selectItems, item);
												if (index > -1) {
													array.splice(index, 1);
												} else {
													array.push(item);
												}
												setSelectItems(array);
											}}>
											<Box
												borderBottomWidth={'1px'}
												borderBottomColor="#E5E6EB"
												alignItems="center"
												justifyContent="center"
												h={pxToDp(54)}>
												<Text
													// color={
													// 	item == selectItems.indexOf(item) > -1
													// 		? mainColor
													// 		: '#1D2129'
													// }
													color={
														contain(selectItems, item) > -1
															? mainColor
															: '#1D2129'
													}
													textAlign="center"
													fontSize={pxToDp(16)}>
													{item.shopName}
												</Text>
											</Box>
										</Pressable>
									);
							  })
							: null}
					</VStack>
				</Modal.Body>
				{props.children}
			</Modal.Content>
		</Modal>
	);
}
