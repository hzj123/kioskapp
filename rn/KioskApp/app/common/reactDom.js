import {Dimensions} from 'react-native';
// 获取竖屏模式的宽度
const deviceWidthDp = Dimensions.get('window').width;
// UI 默认给图是375
const uiWidthPx = 375;
//传入设计稿宽度
export default function pxToDp(uiElementPx) {
	return (uiElementPx * deviceWidthDp) / uiWidthPx + 'px';
}

export function pxToDpNum(uiElementPx) {
	return (uiElementPx * deviceWidthDp) / uiWidthPx;
}
// export default pxToDp;
