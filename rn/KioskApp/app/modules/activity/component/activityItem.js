import * as React from 'react';
import {Box, Text, Button, Pressable, Image} from 'native-base';

import {JFLIcon, pxToDp, ZJComponent} from '../../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

import FastImage from 'react-native-fast-image';

function getHtmlText(html) {
	if (html != null) {
		return (
			html
				?.replace(/<[^>]+>/g, '') //去掉所有的html标记
				?.replace(/↵/g, '') //去掉所有的↵符号
				?.replace(/[\r\n]/g, '') //去掉回车换行
				?.replace(/&nbsp;/g, '') ?? //去掉空格
			''
		);
	} else {
		return '';
	}
}

export default function ActivityItem(props) {
	const {i18n} = useTranslation();

	const [showHot, setShowHot] = React.useState(false);

	React.useState(() => {
		console.log(props.item?.status);
		setShowHot(props.item?.status == 1);
	}, [props.item?.status]);

	return (
		<Pressable
			onPress={
				props.onPress != null
					? () => {
							setShowHot(false);
							props.onPress();
					  }
					: null
			}>
			<Box py={pxToDp(10)} px={pxToDp(16)} flexDir="row" alignItems="center">
				{showHot ? (
					<Box
						zIndex={9}
						bg="#F53F3F"
						w={pxToDp(12)}
						h={pxToDp(12)}
						borderRadius={pxToDp(6)}
						position="absolute"
						top={pxToDp(8)}
						left={pxToDp(8)}
					/>
				) : null}

				<Box display="flex" flex={1} flexDir="column" mr={pxToDp(12)}>
					<Text color="#333" fontSize={pxToDp(14)} noOfLines={1}>
						{(i18n.language == 'zh'
							? props.item?.altTitle
							: props.item?.title) ?? ''}
					</Text>
					<Text color="#666" fontSize={pxToDp(14)} noOfLines={2} mt={pxToDp(8)}>
						{getHtmlText(
							(i18n.language == 'zh'
								? props.item?.altContent
								: props.item?.content) ?? '',
						)}
					</Text>
					<Box flex={1} />
					<Text color="#999" fontSize={pxToDp(14)} noOfLines={1}>
						{(props.item?.startTime ?? '') + '-' + (props.item?.endTime ?? '')}
					</Text>
				</Box>
				<ZJComponent.ZJImage
					size={pxToDp(106)}
					borderRadius={pxToDp(4)}
					uri={props.item?.image ?? ''}
				/>
			</Box>
		</Pressable>
	);
}
