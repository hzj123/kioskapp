import React from 'react';
import {Box, Button, Text} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	mainColor,
	pxToDp,
	ZJLeaveMsgModal,
	ActivityNetwork,
} from '../../common/commonIndex.js';

import {WebView} from 'react-native-webview';
import {useTranslation} from 'react-i18next';

/*活动详情*/
export default function ActivityDetail({navigation, route}) {
	const {t, i18n} = useTranslation();

	// React.useLayoutEffect(() => {
	// 	navigation.setOptions({
	// 		headerTitle: t('order.history_order'),
	// 		headerRight: () => (
	// 			<Button
	// 				onPress={() => onClickNavRightBtn()}
	// 				variant="unstyled"
	// 				_text={{
	// 					color: mainColor,
	// 					fontSize: pxToDp(16),
	// 				}}>
	// 				{t('common.btn_leave_msg')}
	// 			</Button>
	// 		),
	// 	});
	// }, [navigation]);

	const [model, setModel] = React.useState(null);

	React.useLayoutEffect(() => {
		if (route.params?.id > 0) {
			getInfo(route.params?.id);
		}
	}, [route.params?.id]);

	const getInfo = id => {
		global.showLoading();
		ActivityNetwork.detail(id)
			.then(res => {
				global.hiddenLoading();
				setModel(res);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({
					title: err.message,
				});
			});
	};

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('order.history_order'),
			headerRight: () =>
				model ? (
					<ZJLeaveMsgModal promotionId={model?.promotionId}>
						<Text fontSize={pxToDp(16)} color={mainColor}>
							{t('common.btn_leave_msg')}
						</Text>
					</ZJLeaveMsgModal>
				) : null,
		});
	}, [navigation, model]);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle:
				(i18n.language == 'zh' ? model?.altTitle : model?.title) ?? '',
		});
	}, [model]);

	return (
		<Box flex={1} bg="#000">
			<WebView
				originWhitelist={['*']}
				source={{
					html:
						(i18n.language == 'zh' ? model?.altContent : model?.content) ?? '',
				}}
				style={{flex: 1}}
				injectedJavaScript={
					`
                    const meta = document.createElement('meta');
                    meta.setAttribute('content', 'initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width');
                    meta.setAttribute('name', 'viewport');
                    document.getElementsByTagName('head')[0].appendChild(meta);
                    ` +
					`var objs = document.getElementsByTagName('img');for(var i=0;i<objs.length;i++){var img = objs[i];img.style.maxWidth = '100%';img.style.height = 'auto';}`
				}
			/>
		</Box>
	);
}
