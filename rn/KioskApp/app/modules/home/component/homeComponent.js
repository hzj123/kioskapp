import * as React from 'react';
import {Box, Text, Button, Pressable} from 'native-base';

import {
	JFLIcon,
	pxToDp,
	pxToDpNum,
	mainColor,
	ZJComponent,
} from '../../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';
import Swiper from 'react-native-swiper';

const HomeHeaderItem = props => {
	return (
		<Pressable onPress={props.onPress} flex={1}>
			<Box alignItems="center" justifyContent="space-between" h={pxToDp(65)}>
				{props.icon}
				<Text fontSize={pxToDp(14)} color="#333">
					{props.name}
				</Text>
			</Box>
		</Pressable>
	);
};

const ImgItem = props => {
	return (
		<Pressable onPress={props.onPress}>
			<Box flex={1}>
				<ZJComponent.ZJImage
					isDisabled
					width={'100%'}
					height={pxToDp(193)}
					borderRadius={pxToDp(4)}
					uri={props.uri ?? ''}
				/>
			</Box>
		</Pressable>
	);
};

export function HomeHeader(argument) {
	const {t} = useTranslation();

	return (
		<Box py={pxToDp(20)} px={pxToDp(16)}>
			{argument.items?.length > 0 ? (
				<Box w="100%" h={pxToDpNum(193)}>
					<Swiper autoplay={true} activeDotColor={mainColor}>
						{argument.items.map((item, index) => {
							return (
								<ImgItem
									key={index}
									uri={item.image}
									onPress={() => {
										argument.onClickImg ? argument.onClickImg(item) : null;
									}}
								/>
							);
						})}
					</Swiper>
				</Box>
			) : null}

			<Box flexDir="row" justifyContent="space-between" mt={pxToDp(20)}>
				<HomeHeaderItem
					flex={1}
					name={t('home.item_create_order')}
					icon={<JFLIcon.home_create_order />}
					onPress={() => argument.onClickItem(0)}
				/>
				<HomeHeaderItem
					flex={1}
					name={t('home.item_history_order')}
					icon={<JFLIcon.home_history_order />}
					onPress={() => argument.onClickItem(1)}
				/>
				<HomeHeaderItem
					flex={1}
					name={t('home.item_q&a')}
					icon={<JFLIcon.home_normal_question />}
					onPress={() => argument.onClickItem(2)}
				/>
				<HomeHeaderItem
					flex={1}
					name={t('home.item_links')}
					icon={<JFLIcon.home_normal_link />}
					onPress={() => argument.onClickItem(3)}
				/>
			</Box>
		</Box>
	);
}
