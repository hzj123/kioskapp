import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	Pressable,
	Input,
	ScrollView,
	KeyboardAvoidingView,
	StatusBar,
} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	LinksNetwork,
} from '../../common/commonIndex.js';
import {useTranslation} from 'react-i18next';

const Item = props => {
	const {t} = useTranslation();

	const [openModal, setOpenModal] = useState(false);

	return (
		<>
			<Pressable onPress={() => {}}>
				<Box
					bg="#fff"
					my={pxToDp(10)}
					mx={pxToDp(16)}
					py={pxToDp(20)}
					px={pxToDp(16)}
					borderRadius={pxToDp(4)}>
					<Box flexDir="row" alignItems="center">
						<Text
							flex={1}
							color="#333"
							fontSize={pxToDp(16)}
							fontWeight="500"
							noOfLines={1}>
							{props.item?.webName ?? ''}
						</Text>
						<Button
							onPress={props.onPress}
							size={pxToDp(30)}
							variant="unstyled"
							startIcon={<JFLIcon.icon_edit />}
						/>
						<Button
							onPress={() => setOpenModal(true)}
							size={pxToDp(30)}
							variant="unstyled"
							startIcon={<JFLIcon.icon_delete />}
						/>
					</Box>
					<Box mt={pxToDp(12)} flexDir="row">
						<Text
							color="#666"
							fontSize={pxToDp(14)}
							fontWeight="400"
							noOfLines={1}>
							{t('home.links.web_url')}：
						</Text>
						<Pressable onPress={props.onClickUrl}>
							<Text
								color={mainColor}
								fontSize={pxToDp(14)}
								fontWeight="400"
								noOfLines={1}>
								{props.item?.url ?? ''}
							</Text>
						</Pressable>
					</Box>
				</Box>
			</Pressable>
			<ZJComponent.InfoModal
				isOpen={openModal}
				title=""
				info={t('common.delete_confirm_info')}
				onClose={() => setOpenModal(false)}
				clickOKBtn={() => {
					setOpenModal(false);
					props.deleteItem ? props.deleteItem(props.item?.usedLinksId) : null;
				}}
			/>
		</>
	);
};

/*常用链接*/
export default function LinkList({navigation}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('home.links.title'),
			headerRight: () => (
				<Button
					onPress={() => onClickNavRightBtn()}
					variant="unstyled"
					_text={{
						color: mainColor,
						fontSize: pxToDp(16),
					}}>
					{t('common.add')}
				</Button>
			),
		});
	}, [navigation]);

	const onClickNavRightBtn = () => {
		navigation.navigate('AddLink', {
			callBack: () => {
				setTimeout(() => {
					onRefresh();
				}, 400);
			},
		});
	};

	const renderItem = ({item, index}) => {
		return (
			<Item
				item={item}
				deleteItem={id => deleteItem(id)}
				onPress={i => {
					navigation.navigate('AddLink', {
						model: item,
						callBack: () => {
							setTimeout(() => {
								onRefresh();
							}, 400);
						},
					});
				}}
				onClickUrl={() => {
					if (item.url?.length > 0) {
						navigation.navigate('WebDetail', {
							title: item.webName,
							url: item.url,
						});
					}
				}}
			/>
		);
	};

	React.useLayoutEffect(() => {
		setTimeout(() => {
			onRefresh();
		}, 400);
	}, []);

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index);
		}
	}, [loadMore, refreshing]);

	const getInfo = index => {
		LinksNetwork.queryByParam(index)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----

	const deleteItem = id => {
		global.showLoading();
		LinksNetwork.deleteItem(id)
			.then(res => {
				global.hiddenLoading();
				onRefresh();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	return (
		<FlatList
			flex={1}
			bg="#F8F8F8"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			// mt="16px"
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}

/*新增/编辑链接*/
export function AddLink({navigation, route}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		setModel(route.params?.model);
	}, [route.params?.model]);

	/*编辑的模型*/
	const [model, setModel] = React.useState(route.params?.model);

	const [name, setName] = React.useState('');
	const [link, setLink] = React.useState('');

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: model != null ? t('common.edit') : t('common.add'),
		});
		setName(model?.webName);
		setLink(model?.url);
	}, [model]);

	const onClickBottomBtn = () => {
		if (model != null) {
			//编辑
			var params = {...model};
			params.webName = name;
			params.url = link;
			global.showLoading();
			LinksNetwork.update(params)
				.then(res => {
					global.hiddenLoading();
					model.webName = name;
					model.url = link;
					if (route.params?.callBack) {
						route.params?.callBack();
					}
					navigation.goBack();
				})
				.catch(err => {
					global.hiddenLoading();
					global.showToast({title: err.message});
				});
		} else {
			//新增
			global.showLoading();
			var params = {webName: name, url: link, userId: global.user?.userId};
			LinksNetwork.add(params)
				.then(res => {
					global.hiddenLoading();
					if (route.params?.callBack) {
						route.params?.callBack();
					}
					navigation.goBack();
				})
				.catch(err => {
					global.hiddenLoading();
					global.showToast({title: err.message});
				});
		}
	};

	return (
		<KeyboardAvoidingView
			flex={1}
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
			<ScrollView>
				<Box
					alignItems="center"
					mt={pxToDp(20)}
					mx={pxToDp(16)}
					bg="#fff"
					borderRadius={pxToDp(10)}>
					<Box
						w="100%"
						px={pxToDp(16)}
						h={pxToDp(54)}
						alignItems="center"
						justifyContent="center"
						flexDir="row">
						<Text w={pxToDp(90)} color="#333" fontSize={pxToDp(14)}>
							{t('home.links.web_name')}
						</Text>
						<Input
							flex={1}
							fontSize={pxToDp(16)}
							variant="unstyled"
							placeholderTextColor="#999999"
							color="#333333"
							placeholder={t('common.please_enter')}
							value={name}
							onChangeText={setName}
						/>
					</Box>
					<Box
						w="100%"
						px={pxToDp(16)}
						h={pxToDp(54)}
						alignItems="center"
						justifyContent="center"
						flexDir="row">
						<Text w={pxToDp(90)} color="#333" fontSize={pxToDp(14)}>
							{t('home.links.web_url')}
						</Text>
						<Input
							flex={1}
							fontSize={pxToDp(16)}
							variant="unstyled"
							placeholderTextColor="#999999"
							color="#333333"
							placeholder={t('common.please_enter')}
							value={link}
							onChangeText={setLink}
						/>
					</Box>
				</Box>
				<ZJComponent.ThemeButton
					flex={1}
					mt={pxToDp(50)}
					mx={pxToDp(16)}
					h={pxToDp(42)}
					title={t('common.btn_submit')}
					isDisabled={!(name?.length > 0 && link?.length > 0)}
					onPress={() => onClickBottomBtn()}
				/>
			</ScrollView>
		</KeyboardAvoidingView>
	);
}

import {WebView} from 'react-native-webview';

/*网络网页*/
export function WebDetail({navigation, route}) {
	const [url, setUrl] = React.useState(route.params?.url);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: route.params?.title,
		});
	}, [route.params?.title]);

	return (
		<Box flex={1} bg="#000">
			<StatusBar hidden={false} />
			<WebView
				originWhitelist={['*']}
				source={{
					uri: url,
				}}
				style={{flex: 1}}
				injectedJavaScript={
					`
                    const meta = document.createElement('meta');
                    meta.setAttribute('content', 'initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width');
                    meta.setAttribute('name', 'viewport');
                    document.getElementsByTagName('head')[0].appendChild(meta);
                    ` +
					`var objs = document.getElementsByTagName('img');for(var i=0;i<objs.length;i++){var img = objs[i];img.style.maxWidth = '100%';img.style.height = 'auto';}`
				}
			/>
		</Box>
	);
}
