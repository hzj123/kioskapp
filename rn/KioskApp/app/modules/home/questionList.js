import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	ScrollView,
	Pressable,
	Input,
} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJTabScroll,
	ZJComponent,
	UserNetwork,
} from '../../common/commonIndex.js';

import {RefreshControl} from 'react-native';

import OrderItem from '../order/component/orderItem.js';

import {useTranslation} from 'react-i18next';

export default function QuestionList({navigation}) {
	const {t, i18n} = useTranslation();
	const [data, setData] = React.useState([]);

	/*搜索的字段*/
	const [keyWord, setKeyWord] = React.useState(null);

	/*调用便会刷新*/
	const [toRefresh, setToRefresh] = React.useState(false);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.Q&A'),
		});
		getQuestionType();
	}, [navigation]);

	/*获取常见问题类型*/
	const getQuestionType = () => {
		global.showLoading();
		UserNetwork.getQuestionType()
			.then(res => {
				global.hiddenLoading();
				setData(
					res.map((item, index) => {
						return {
							title:
								(i18n.language == 'zh'
									? item.altQuestionType
									: item.questionType) ?? '',
							page: props => (
								<QuestionOneList
									key={item + index}
									navigation={navigation}
									info={item}
									{...props}
								/>
							),
						};
					}),
				);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	return (
		<Box h="100%" bg="#F8F8F8">
			<Box
				w="100%"
				alignItems="center"
				flexDir="row"
				mt={pxToDp(12)}
				px={pxToDp(16)}
				py={pxToDp(8)}>
				<Box flex={1} bg="#EEEEEE" borderRadius={pxToDp(2)}>
					<Input
						h={pxToDp(36)}
						variant="unstyled"
						placeholderTextColor="#999999"
						placeholder={t('common.search_placeholder')}
						fontSize={pxToDp(14)}
						// value={keyWord}
						// onChangeText={setKeyWord}
						returnKeyType="search"
						onSubmitEditing={e => {
							setKeyWord(e?.nativeEvent?.text);
							setToRefresh(!toRefresh);
						}}
						InputLeftElement={
							<Box
								size={pxToDp(40)}
								alignItems="center"
								justifyContent="center">
								<JFLIcon.search_icon />
							</Box>
						}
					/>
				</Box>
			</Box>
			<ZJTabScroll
				data={data}
				flex={1}
				isLeft
				keyWord={keyWord}
				toRefresh={toRefresh}
			/>
		</Box>
	);
}

/*问题item*/
const QuestionItem = props => {
	const {i18n} = useTranslation();

	return (
		<Pressable onPress={props.onPress}>
			<Box
				bg="#fff"
				my={pxToDp(10)}
				mx={pxToDp(16)}
				py={pxToDp(20)}
				px={pxToDp(16)}
				borderRadius={pxToDp(4)}>
				<Box flexDir="row">
					<JFLIcon.question_title />
					<Text
						flex={1}
						ml={pxToDp(8)}
						color="#333"
						fontSize={pxToDp(14)}
						fontWeight="500"
						noOfLines={1}>
						{(i18n.language == 'zh'
							? props.item?.altProblem
							: props.item?.problem) ?? ''}
					</Text>
				</Box>
				<Box mt={pxToDp(12)} flexDir="row">
					<JFLIcon.question_answer />
					<Text
						flex={1}
						ml={pxToDp(8)}
						color="#666"
						fontSize={pxToDp(14)}
						fontWeight="400"
						noOfLines={3}>
						{(i18n.language == 'zh'
							? props.item?.altAnswers
							: props.item?.answers) ?? ''}
					</Text>
				</Box>
			</Box>
		</Pressable>
	);
};

/*单个列表的常用问题*/
function QuestionOneList({
	navigation,
	info,
	route,
	selected,
	keyWord,
	toRefresh,
}) {
	const {t, i18n} = useTranslation();

	React.useLayoutEffect(() => {
		console.log(toRefresh);
		if (selected) {
			setTimeout(() => {
				onRefresh();
			}, 400);
		}
	}, [toRefresh]);

	// React.useLayoutEffect(() => {
	// 	if (selected) {
	// 		setTimeout(() => {
	// 			onRefresh();
	// 		}, 400);
	// 	}
	// }, [keyWord]);

	React.useLayoutEffect(() => {
		if (selected) {
			setTimeout(() => {
				onRefresh();
			}, 400);
		}
	}, [selected]);

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1, keyWord);
	}, [page, keyWord]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index, keyWord);
		}
	}, [loadMore, keyWord]);

	const getInfo = (index, keyword) => {
		UserNetwork.getCommonQuestion(info?.questionTypeId, keyword, index)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setData(newData);
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----

	const renderItem = ({item, index}) => {
		return (
			<QuestionItem
				item={item}
				onPress={() => {
					navigation.navigate('QuestionDetail', {
						model: item,
					});
				}}
			/>
		);
	};

	return (
		<FlatList
			w="100%"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}
