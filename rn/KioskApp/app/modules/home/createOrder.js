import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Image,
	Pressable,
	ScrollView,
	KeyboardAvoidingView,
	Checkbox,
	Button,
	TextArea,
	AspectRatio,
} from 'native-base';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	ZJChoosePhoto,
	UserNetwork,
	OrderNetwork,
	FileNetwork,
} from '../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

export default function CreateOrder({navigation, route}) {
	const {t, i18n} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('order.create_order'),
		});
	}, [navigation]);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<Button
					onPress={() => onClickNavRightBtn()}
					variant="unstyled"
					isDisabled={
						!(
							selectStoreArray?.length > 0 &&
							selectUrgency != null &&
							type != null &&
							title?.length > 0
						)
					}
					_text={{
						color: mainColor,
						fontSize: pxToDp(16),
					}}>
					{t('common.submit')}
				</Button>
			),
		});
	});

	/*商店*/
	const [storeArray, setStoreArray] = React.useState([]);

	/*选中的商店*/
	const [selectStoreArray, setSelectStoreArray] = React.useState([]);

	/*紧急程度:*/
	const [urgency, setUrgency] = React.useState([
		{
			title:
				t('order.urgency_general') + '(' + t('order.urgency_general_des') + ')',
			id: 2,
		},
		{
			title:
				t('order.urgency_moderate') +
				'(' +
				t('order.urgency_moderate_des') +
				')',
			id: 1,
		},
		{
			title: t('order.urgency_very') + '(' + t('order.urgency_very_des') + ')',
			id: 0,
		},
	]);

	/*选中的紧急程度*/
	const [selectUrgency, setSelectUrgency] = React.useState(urgency[0]);

	/*所有的类别*/
	const [allType, setAllType] = React.useState([]);

	/*选中的类别*/
	const [type, setType] = React.useState();

	const [title, setTitle] = React.useState();

	const [content, setContent] = React.useState();

	const [contentPicsList, setContentPicsList] = React.useState([]);

	const [openModal, setOpenModal] = React.useState(false);

	const [openStoreModal, setOpenStoreModal] = React.useState(false);

	const getInfo = () => {
		global.showLoading();
		Promise.all([UserNetwork.getAllShop(), OrderNetwork.orderTypeList()])
			.then(res => {
				global.hiddenLoading();
				setStoreArray(res[0]);
				setAllType(res[1]);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	React.useLayoutEffect(() => {
		getInfo();
	}, [navigation]);

	/* 点击提交按钮 */
	const onClickNavRightBtn = () => {
		var params = {
			createUserId: global.user?.userId,
			customerId: global.user?.customerId,
			shopIdList: selectStoreArray.map(i => i.shopId),
			urgency: selectUrgency.id,
			ticketTypeId: type.ticketTypeId,
			title: title,
			content: content,
			contentPicsList: contentPicsList,
			status: 0,
		};
		global.showLoading();
		OrderNetwork.add(params)
			.then(res => {
				global.hiddenLoading();
				if (route.params?.callback) {
					route.params?.callback();
				}
				navigation.goBack();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	const getImage = img => {
		global.showLoading();
		FileNetwork.fileUpload(img)
			.then(res => {
				global.hiddenLoading();
				setContentPicsList([...contentPicsList, res.fileUrl]);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	const onClickDeleteImg = idx => {
		console.log(idx);
		if (idx < contentPicsList.length) {
			var newArray = [...contentPicsList];
			newArray.splice(idx, 1);
			setContentPicsList(newArray);
		}
	};

	return (
		<KeyboardAvoidingView
			display={'flex'}
			flex={1}
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
			<ScrollView showsVerticalScrollIndicator={false}>
				{/*<Box pt={pxToDp(20)} px={pxToDp(16)} bg="#fff" flexDir="row">
					<Text
						fontSize={pxToDp(14)}
						color="#F53F3F"
						position="absolute"
						top={pxToDp(14)}
						left={pxToDp(12)}>
						*
					</Text>
					<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#000">
						{t('order.store')}:
					</Text>
					<Box flex={1}>
						{storeArray.map((item, index) => {
							return (
								<ZJComponent.ZJCheckBox
									key={item + index}
									IsChecked={
										selectStoreArray
											.map(item => item.shopId)
											.indexOf(item.shopId) > -1
									}
									title={item.shopName}
									onPress={() => {
										var array = [...selectStoreArray];
										var index = selectStoreArray
											.map(item => item.shopId)
											.indexOf(item.shopId);
										if (index > -1) {
											array.splice(index, 1);
										} else {
											array.push(item);
										}
										setSelectStoreArray(array);
									}}
								/>
							);
						})}
					</Box>
				</Box>*/}
				<Pressable onPress={() => setOpenStoreModal(true)}>
					<Box py={pxToDp(20)} px={pxToDp(16)} bg="#F8F8F8" flexDir="row">
						<Text
							fontSize={pxToDp(14)}
							color="#F53F3F"
							position="absolute"
							top={pxToDp(14)}
							left={pxToDp(12)}>
							*
						</Text>
						<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#333">
							{t('order.store')}:
						</Text>
						{selectStoreArray.length > 0 ? (
							<Text
								flex={1}
								fontSize={pxToDp(14)}
								color="#000"
								textAlign="right">
								{selectStoreArray.map(item => item.shopName).join(';\n')}
							</Text>
						) : (
							<Text
								flex={1}
								fontSize={pxToDp(14)}
								color="#666"
								textAlign="right">
								{t('common.please_select')}
							</Text>
						)}
						<Box ml={pxToDp(12)}>
							<JFLIcon.right_arrow />
						</Box>
					</Box>
				</Pressable>
				<Box pt={pxToDp(20)} px={pxToDp(16)} bg="#ccc" flexDir="row">
					<Text
						fontSize={pxToDp(14)}
						color="#F53F3F"
						position="absolute"
						top={pxToDp(14)}
						left={pxToDp(12)}>
						*
					</Text>
					<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#333">
						{t('order.emergency_degree')}:
					</Text>
					<Box flex={1}>
						{urgency.map((item, index) => {
							return (
								<ZJComponent.ZJCheckBox
									key={item + index}
									IsChecked={selectUrgency?.title === item.title}
									title={item.title}
									onPress={() => {
										setSelectUrgency(item);
									}}
								/>
							);
						})}
					</Box>
				</Box>
				<Pressable onPress={() => setOpenModal(true)}>
					<Box
						px={pxToDp(16)}
						bg="#F8F8F8"
						flexDir="row"
						h={pxToDp(60)}
						alignItems="center">
						<Text
							fontSize={pxToDp(14)}
							color="#F53F3F"
							position="absolute"
							top={pxToDp(14)}
							left={pxToDp(12)}>
							*
						</Text>
						<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#333">
							{t('order.category')}:
						</Text>
						{type != null ? (
							<Text
								flex={1}
								fontSize={pxToDp(14)}
								color="#000"
								textAlign="right">
								{(i18n.language == 'zh' ? type.altType : type.type) ?? ''}
							</Text>
						) : (
							<Text
								flex={1}
								fontSize={pxToDp(14)}
								color="#666"
								textAlign="right">
								{t('common.please_select')}
							</Text>
						)}
						<Box ml={pxToDp(12)}>
							<JFLIcon.right_arrow />
						</Box>
					</Box>
				</Pressable>
				<Box
					px={pxToDp(16)}
					bg="#ccc"
					flexDir="column"
					pt={pxToDp(30)}
					pb={pxToDp(20)}>
					<Text
						fontSize={pxToDp(14)}
						color="#F53F3F"
						position="absolute"
						top={pxToDp(24)}
						left={pxToDp(12)}>
						*
					</Text>
					<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#333">
						{t('order.title')}:
					</Text>
					<Box bg="#fff" mt={pxToDp(10)} borderRadius={pxToDp(4)}>
						<TextArea
							maxLength={50}
							variant="unstyled"
							placeholderTextColor="#999"
							placeholder={t('placeholder.create_order_title')}
							fontSize={pxToDp(14)}
							value={title}
							onChangeText={e => setTitle(e)}
						/>
					</Box>
				</Box>
				<Box
					px={pxToDp(16)}
					bg="#fff"
					flexDir="column"
					pt={pxToDp(30)}
					pb={pxToDp(20)}>
					<Text fontSize={pxToDp(14)} w={pxToDp(93)} color="#333">
						{t('order.content')}:
					</Text>
					<Box
						bg="#F8F8F8"
						mt={pxToDp(10)}
						pt={pxToDp(8)}
						pb={pxToDp(20)}
						borderRadius={pxToDp(4)}>
						<TextArea
							maxLength={200}
							px={pxToDp(10)}
							variant="unstyled"
							placeholderTextColor="#999"
							placeholder={t('placeholder.create_order_content')}
							fontSize={pxToDp(14)}
							value={content}
							onChangeText={e => setContent(e)}
						/>
						<Box px={pxToDp(8)} flexDir="row" flexWrap="wrap">
							{contentPicsList.map((item, index) => {
								return (
									<Box w="33%" p={pxToDp(4)} key={item + index}>
										<AspectRatio ratio={{base: 1, md: 1}} flex={1}>
											<>
												<ZJComponent.ZJImage
													flex={1}
													borderRadius={pxToDp(2)}
													uri={item}
												/>
												<Pressable
													position="absolute"
													right={0}
													onPress={() => onClickDeleteImg(index)}>
													<JFLIcon.icon_delete />
												</Pressable>
											</>
										</AspectRatio>
									</Box>
								);
							})}
							{contentPicsList.length < 9 ? (
								<Box w="33%" p={pxToDp(4)}>
									<AspectRatio ratio={{base: 1, md: 1}} flex={1}>
										<ZJChoosePhoto getImage={getImage}>
											<Box
												flex={1}
												alignItems="center"
												justifyContent="center"
												borderRadius={pxToDp(2)}
												bg="#fff">
												<JFLIcon.icon_add />
											</Box>
										</ZJChoosePhoto>
									</AspectRatio>
								</Box>
							) : null}
						</Box>
					</Box>
				</Box>
				<ZJComponent.ChooseOrderTypeList
					isOpen={openModal}
					list={allType}
					selectItem={type}
					onClose={() => setOpenModal(false)}
					clickOKBtn={item => {
						setOpenModal(false);
						setType(item);
					}}
				/>
				<ZJComponent.ChooseOrderStoreList
					isOpen={openStoreModal}
					list={storeArray}
					selectItems={selectStoreArray}
					onClose={() => setOpenStoreModal(false)}
					clickOKBtn={items => {
						setOpenStoreModal(false);
						setSelectStoreArray(items);
					}}
				/>
			</ScrollView>
		</KeyboardAvoidingView>
	);
}
