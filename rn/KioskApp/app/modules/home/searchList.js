import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	Image,
	Input,
	Pressable,
	ScrollView,
	KeyboardAvoidingView,
	FlatList,
} from 'native-base';

import {RefreshControl, Keyboard} from 'react-native';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	ZJTabScroll,
	OrderNetwork,
	ActivityNetwork,
} from '../../common/commonIndex.js';

import {HomeHeader} from './component/homeComponent.js';
import OrderItem from '../order/component/orderItem.js';
import ActivityItem from '../activity/component/activityItem.js';
import {useTranslation} from 'react-i18next';

export default function SearchList({navigation}) {
	const {t} = useTranslation();

	const [typeList, setTypeList] = React.useState([
		{
			title: t('home.search.order'),
			page: props => (
				<SearchOneList key="0" navigation={navigation} type={0} {...props} />
			),
		},
		{
			title: t('home.search.activity'),
			page: props => (
				<SearchOneList key="1" navigation={navigation} type={1} {...props} />
			),
		},
	]);

	/*搜索框中的字段*/
	const [keyWord, setKeyWord] = React.useState(null);
	/*搜索时的字段*/
	const [searchKeyWord, setSearchKeyWord] = React.useState(null);

	/*调用便会刷新*/
	const [toRefresh, setToRefresh] = React.useState(false);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerStyle: {
				backgroundColor: '#F8F8F8',
			},
			headerTitle: props => {
				return (
					<Box bg="#EEEEEE" borderRadius={pxToDp(2)}>
						<Input
							w={pxToDp(240)}
							h={pxToDp(36)}
							pl={0}
							variant="unstyled"
							placeholderTextColor="#999999"
							placeholder={t('common.search_placeholder')}
							fontSize={pxToDp(14)}
							value={keyWord}
							onChangeText={setKeyWord}
							returnKeyType="search"
							onSubmitEditing={e => {
								// console.log(e.nativeEvent);
								setSearchKeyWord(e?.nativeEvent?.text);
								setToRefresh(!toRefresh);
							}}
							InputLeftElement={
								<Box
									size={pxToDp(40)}
									alignItems="center"
									justifyContent="center">
									<JFLIcon.search_icon />
								</Box>
							}
						/>
					</Box>
				);
			},
			headerRight: () => (
				<Pressable
					onPress={() => {
						// global.showToast({title: 'development...'});
						Keyboard.dismiss();
						setSearchKeyWord(keyWord);
						setToRefresh(!toRefresh);
					}}>
					<Box
						position="relative"
						alignItems="center"
						justifyContent="center"
						h={pxToDp(44)}
						minW={pxToDp(44)}
						_text={{
							color: mainColor,
							fontSize: pxToDp(15),
							textAlign: 'right',
						}}>
						{t('common.search')}
					</Box>
				</Pressable>
			),
			headerTitleAlign: 'left',
		});
	}, [navigation, keyWord, toRefresh]);

	return (
		<ZJTabScroll
			data={typeList}
			flex={1}
			keyWord={searchKeyWord}
			toRefresh={toRefresh}
		/>
	);
}

/*单个列表的搜索*/
function SearchOneList({
	navigation,
	type,
	route,
	selected,
	keyWord,
	toRefresh,
}) {
	React.useLayoutEffect(() => {
		console.log(toRefresh);
		if (selected) {
			setTimeout(() => {
				onRefresh();
			}, 400);
		}
	}, [toRefresh]);

	// React.useLayoutEffect(() => {
	// 	console.log(keyWord);
	// 	if (selected) {
	// 		setTimeout(() => {
	// 			onRefresh();
	// 		}, 400);
	// 	}
	// }, [keyWord]);

	React.useLayoutEffect(() => {
		if (selected) {
			setTimeout(() => {
				onRefresh();
			}, 400);
		}
	}, [selected]);

	const renderItem = ({item, index}) => {
		return type === 0 ? (
			<OrderItem
				item={item}
				onPress={() => {
					console.log(data);
					// data[index].notReadNumber = 0;
					navigation.navigate('OrderDetail', {id: item.ticketId});
				}}
			/>
		) : (
			<ActivityItem
				item={item}
				onPress={() => {
					data[index].status = 2;
					navigation.navigate('ActivityDetail', {
						id: item?.promotionId,
					});
				}}
			/>
		);
	};

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1, keyWord);
	}, [page, keyWord]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index, keyWord);
		}
	}, [loadMore, refreshing, keyWord]);

	const getInfo = (index, keyWord) => {
		if (type === 0) {
			_getOrderList(index, keyWord);
		} else {
			_getActivityList(index, keyWord);
		}
	};

	const _getOrderList = (index, keyWord) => {
		OrderNetwork.findByParam(index, keyWord)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	const _getActivityList = (index, keyWord) => {
		ActivityNetwork.findList(index, keyWord)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----
	return (
		<FlatList
			w="100%"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}
