import React, {useState, useLayoutEffect} from 'react';
import {Box, Text, ScrollView} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
} from '../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

/*问题详情*/
export default function QuestionDetail({navigation, route}) {
	const {t, i18n} = useTranslation();

	const [model, setModel] = React.useState(route.params?.model);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('common.title_detail'),
		});
	}, [navigation]);

	return (
		<ScrollView w="100%" bg="#F8F8F8" showsVerticalScrollIndicator={false}>
			<Box flex={1} bg="#F8F8F8" p={pxToDp(16)}>
				<Box>
					<Text color="#333" fontSize={pxToDp(16)}>
						{(i18n.language == 'zh' ? model?.altProblem : model?.problem) ?? ''}
					</Text>
				</Box>
				<Box mt={pxToDp(12)}>
					<Text color="#666" fontSize={pxToDp(14)}>
						{(i18n.language == 'zh' ? model?.altAnswers : model?.answers) ?? ''}
					</Text>
				</Box>
			</Box>
		</ScrollView>
	);
}
