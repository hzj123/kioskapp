import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	Image,
	Input,
	Pressable,
	ScrollView,
	KeyboardAvoidingView,
	FlatList,
} from 'native-base';

import {DeviceEventEmitter} from 'react-native';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
	OrderNetwork,
	ActivityNetwork,
} from '../../common/commonIndex.js';

import {HomeHeader} from './component/homeComponent.js';
import OrderItem from '../order/component/orderItem.js';

import {useTranslation} from 'react-i18next';
import Swiper from 'react-native-swiper';

export default function HomeScreen({navigation}) {
	const {t} = useTranslation();

	//---获取未读消息---start----
	React.useEffect(() => {
		const msgSubscription = DeviceEventEmitter.addListener('myMsgNum', msg => {
			setUnreadNum(msg ?? 0);
		});
		return function cleanup() {
			msgSubscription?.remove();
		};
	}, [navigation]);

	useFocusEffect(
		React.useCallback(() => {
			getAllNum();
		}, []),
	);

	/*未读消息数*/
	var [unreadNum, setUnreadNum] = React.useState(0);

	/*获取所有的未读消息数*/
	const getAllNum = () => {
		Promise.all([
			UserNetwork.getMessagesCount(),
			OrderNetwork.getMessageNum(),
			ActivityNetwork.getMessageNum(),
		])
			.then(res => {
				DeviceEventEmitter.emit('myMsgNum', res[0].messagesCount);
				DeviceEventEmitter.emit('orderNum', res[1]);
				DeviceEventEmitter.emit('activityNum', res[2]);
			})
			.catch(err => {});
	};
	//---获取未读消息---end----

	//---数据获取相关---starty----

	const [banner, setBanner] = React.useState([]);

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getRefresh(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getLoadMore(index);
		}
	}, [loadMore, refreshing]);

	var hhh = false;
	const getRefresh = index => {
		Promise.all([
			ActivityNetwork.findList(1, null, 3),
			OrderNetwork.findByParam(1),
		])
			.then(res => {
				setRefreshing(false);
				setBanner(res[0]);
				var newData = res[1] ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	const getLoadMore = index => {
		OrderNetwork.findByParam(index)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----

	React.useLayoutEffect(() => {
		setTimeout(() => {
			onRefresh();
		}, 400);
	}, []);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerStyle: {
				backgroundColor: '#F8F8F8',
			},
			headerTitle: props => {
				return (
					<Box alignItems="center" flexDir="row">
						<Pressable
							onPress={() => {
								navigation.navigate('SearchList');
							}}>
							<Box
								w={pxToDp(300)}
								h={pxToDp(36)}
								alignItems="center"
								px={pxToDp(14)}
								bg="#EEEEEE"
								borderRadius={pxToDp(2)}
								flexDir="row">
								<JFLIcon.search_icon />
								<Text color="999999" fontSize={pxToDp(14)} ml={pxToDp(10)}>
									{t('common.search_placeholder')}
								</Text>
							</Box>
						</Pressable>
						<Pressable onPress={() => navigation.navigate('MessageTabList')}>
							<Box
								position="relative"
								alignItems="center"
								justifyContent="center"
								size={pxToDp(40)}>
								<JFLIcon.msg_icon />
								{unreadNum > 0 ? (
									<Box
										position="absolute"
										top={0}
										right={0}
										px={pxToDp(6)}
										h={pxToDp(16)}
										borderRadius={pxToDp(8)}
										bg="#F53F3F">
										<Text fontSize={pxToDp(11)} color="#fff">
											{unreadNum}
										</Text>
									</Box>
								) : null}
							</Box>
						</Pressable>
					</Box>
				);
			},
			headerTitleAlign: 'left',
		});
	});

	const ListHeaderComponent = React.useCallback(() => {
		var _this = this;
		return (
			<HomeHeader
				items={banner}
				onClickImg={item => {
					navigation.navigate('ActivityDetail', {
						id: item?.promotionId,
					});
				}}
				onClickItem={index => {
					if (0 === index) {
						navigation.navigate('CreateOrder', {
							callback: () => {
								setTimeout(() => {
									onRefresh();
								}, 400);
							},
						});
					} else if (1 === index) {
						navigation.navigate('HistoryOrderList');
					} else if (2 === index) {
						navigation.navigate('QuestionList');
					} else if (3 === index) {
						navigation.navigate('LinkList');
					}
				}}
			/>
		);
	}, [banner]);

	const renderItem = ({item, index}) => {
		return (
			<OrderItem
				item={item}
				onPress={() => {
					data[index].notReadNumber = 0;
					navigation.navigate('OrderDetail', {id: item.ticketId});
				}}
			/>
		);
	};

	return (
		<FlatList
			flex={1}
			bg="#F8F8F8"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			// mt="16px"
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListHeaderComponent={ListHeaderComponent}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			ListEmptyComponent={<ZJComponent.EmptyView pt="20%" />}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}
