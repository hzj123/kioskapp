import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	Image,
	Input,
	Pressable,
	ScrollView,
	KeyboardAvoidingView,
	FlatList,
} from 'native-base';

import {RefreshControl} from 'react-native';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	ZJTabScroll,
	UserNetwork,
} from '../../common/commonIndex.js';

import {HomeHeader} from './component/homeComponent.js';
import OrderItem from '../order/component/orderItem.js';
import ActivityItem from '../activity/component/activityItem.js';

import {useTranslation} from 'react-i18next';

/*消息列表*/
export default function MessageTabList({navigation}) {
	const {t} = useTranslation();

	const [typeList, setTypeList] = React.useState([
		{
			title: t('my.msg.tab_order'),
			page: props => (
				<MessageList navigation={navigation} type={0} {...props} />
			),
		},
		{
			title: t('my.msg.tab_system'),
			page: props => (
				<MessageList navigation={navigation} type={1} {...props} />
			),
		},
	]);

	/*搜索的字段*/
	const [keyWord, setKeyWord] = React.useState(null);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.message'),
		});
	}, [navigation]);

	return <ZJTabScroll data={typeList} flex={1} itemMinW={pxToDp(100)} />;
}

const SystemItem = props => {
	const {t} = useTranslation();
	return (
		<Pressable onPress={props.onPress}>
			<Box
				bg="#fff"
				my={pxToDp(10)}
				mx={pxToDp(16)}
				py={pxToDp(20)}
				px={pxToDp(16)}
				borderRadius={pxToDp(4)}>
				<Text
					color="#333"
					fontSize={pxToDp(14)}
					fontWeight="400"
					noOfLines={1}
					minH={pxToDp(20)}>
					{props.item?.title ?? t('my.msg.tab_system')}
				</Text>
				<Text
					color="#999"
					fontSize={pxToDp(12)}
					fontWeight="400"
					noOfLines={1}
					mt={pxToDp(4)}
					minH={pxToDp(20)}>
					{props.item?.createTime ?? ''}
				</Text>
				<Text
					color="#333"
					fontSize={pxToDp(14)}
					fontWeight="400"
					noOfLines={2}
					mt={pxToDp(8)}
					minH={pxToDp(20)}>
					{props.item?.content ?? ''}
				</Text>
			</Box>
		</Pressable>
	);
};

/*系统消息*/
function MessageList({navigation, route, selected, type}) {
	React.useLayoutEffect(() => {
		if (selected) {
			setTimeout(() => {
				onRefresh();
			}, 400);
		}
	}, [selected]);

	const renderItem = React.useCallback(
		({item, index}) => {
			return type == 1 ? (
				<SystemItem
					item={item}
					onPress={() =>
						navigation.navigate('MessageSystemDetail', {
							model: item,
						})
					}
				/>
			) : (
				<OrderItem
					onPress={() => {
						data[index].notReadNumber = 0;
						navigation.navigate('OrderDetail', {id: item.ticketId});
					}}
				/>
			);
		},
		[type],
	);

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index);
		}
	}, [loadMore, refreshing]);

	const getInfo = index => {
		UserNetwork.getPageMessages(type, index)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----

	return (
		<FlatList
			w="100%"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}

/*系统消息详情*/
export function MessageSystemDetail({navigation, route}) {
	const {t} = useTranslation();

	const [model, setModel] = React.useState(route.params?.model);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.msg.tab_system'),
		});
	}, [navigation]);

	return (
		<Box bg="#F8F8F8" flex={1} px={pxToDp(16)} py={pxToDp(20)}>
			<ScrollView w="100%" bg="#fff" borderRadius={pxToDp(4)}>
				<Box mx={pxToDp(16)} my={pxToDp(20)} alignItems="center">
					<Box
						bg="#F8F8F8"
						px={pxToDp(16)}
						h={pxToDp(28)}
						alignItems="center"
						justifyContent="center">
						<Text fontSize={pxToDp(14)} color="#999">
							{model?.createTime ?? ''}
						</Text>
					</Box>
					<Box flexDir="row" mt={pxToDp(32)}>
						<Box
							mr={pxToDp(10)}
							size={pxToDp(40)}
							borderRadius={pxToDp(4)}
							bg={mainColor}
							alignItems="center"
							justifyContent="center">
							<JFLIcon.service_photo />
						</Box>
						<Box p={pxToDp(12)} bg="#F8F8F8" flex={1}>
							<Text fontSize={pxToDp(14)} color="#333">
								{model?.content ?? ''}
							</Text>
						</Box>
					</Box>
				</Box>
			</ScrollView>
		</Box>
	);
}
