import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	ScrollView,
	Image,
	Input,
	Pressable,
	Button,
	KeyboardAvoidingView,
} from 'native-base';

import {Keyboard} from 'react-native';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
	FileNetwork,
	ZJChoosePhoto,
} from '../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

/*个人资料*/
export default function Personal({navigation}) {
	const {t, i18n} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.personal_data'),
		});
	});

	/*语言选择弹窗*/
	const [openModal, setOpenModal] = React.useState(false);

	const PhotoItem = props => {
		return (
			<Pressable onPress={props.onPress} disabled={props.disabled}>
				<Box
					w="100%"
					h={pxToDp(54)}
					flexDir="row"
					alignItems="center"
					px={pxToDp(16)}>
					<Text fontSize={pxToDp(16)} color="#666" w={pxToDp(88)}>
						{props.title}
					</Text>
					<Box flex={1} />
					<ZJComponent.ZJImage
						isDisabled
						mr={pxToDp(10)}
						size={pxToDp(30)}
						borderRadius={pxToDp(15)}
						uri={props.image ?? ''}
					/>
					<JFLIcon.right_arrow />
				</Box>
			</Pressable>
		);
	};

	const SelectItem = props => {
		return (
			<Pressable onPress={props.onPress} disabled={props.disabled}>
				<Box
					w="100%"
					h={pxToDp(54)}
					flexDir="row"
					alignItems="center"
					px={pxToDp(16)}>
					<Text fontSize={pxToDp(16)} color="#666" w={pxToDp(88)}>
						{props.title}
					</Text>
					<Box flex={1} />
					{props.info ? (
						<Text fontSize={pxToDp(14)} color="#333">
							{props.info}
						</Text>
					) : (
						<Text fontSize={pxToDp(14)} color="#999">
							{props.placeholder}
						</Text>
					)}
					<Box w={pxToDp(10)} />
					<JFLIcon.right_arrow />
				</Box>
			</Pressable>
		);
	};

	const onClickItem = index => {
		if (1 === index) {
			navigation.navigate('PersonalEditText', {
				type: index,
				info: username,
				callback: info => {
					setUsername(info);
					global.user.nickname = info;
				},
			});
		} else if (2 === index) {
			navigation.navigate('PersonalEditText', {
				type: index,
				info: mobile,
				callback: info => {
					setMobile(info);
					global.user.mobile = info;
				},
			});
		} else if (3 === index) {
			navigation.navigate('PersonalEditText', {
				type: index,
				info: email,
				callback: info => {
					setEmail(info);
					global.user.email = info;
				},
			});
		}
	};

	const languageArray = [t('lan_zh'), t('lan_en')];

	const [currentLan, setCurrentLan] = React.useState(i18n.language);

	const [user, setUser] = React.useState(global.user);
	const [photo, setPhoto] = React.useState(global.user?.photo);
	const [username, setUsername] = React.useState(global.user?.nickname);
	const [mobile, setMobile] = React.useState(global.user?.mobile);
	const [email, setEmail] = React.useState(global.user?.email);

	React.useEffect(() => {
		_getUserInfo();
	}, [navigation]);

	React.useEffect(() => {
		setUsername(user?.nickname);
		setMobile(user?.mobile);
		setEmail(user?.email);
	}, [user]);

	/*获取用户信息*/
	const _getUserInfo = () => {
		global.showLoading();
		UserNetwork.findUserId()
			.then(res => {
				global.hiddenLoading();
				global.user = res;
				setUser(res);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	/*更改语言*/
	const changeLanguage = item => {
		// updateLanguage
		setOpenModal(false);
		setCurrentLan(item);
		var lan = item == t('lan_zh') ? 'zh' : 'en';
		setCurrentLan(lan);
		i18n.changeLanguage(lan);

		global.showLoading();
		Promise.all([
			storage.save({
				key: 'language',
				data: lan,
			}),
			UserNetwork.updateLanguage(lan == 'zh' ? 1 : 2),
		])
			.then(res => global.hiddenLoading())
			.catch(err => global.hiddenLoading());
	};

	const getImage = img => {
		global.showLoading();
		var imgUrl = '';
		FileNetwork.fileUpload(img)
			.then(res => {
				imgUrl = res.fileUrl;
				return UserNetwork.updateUser({photo: imgUrl});
			})
			.then(res => {
				global.hiddenLoading();
				setPhoto(imgUrl);
				global.user.photo = imgUrl;
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	return (
		<ScrollView w="100%" bg="#F8F8F8">
			<Box mx={pxToDp(16)} bg="#fff" borderRadius={pxToDp(8)} mt={pxToDp(30)}>
				<ZJChoosePhoto getImage={getImage}>
					<PhotoItem title={t('my.avatar')} disabled image={photo} />
				</ZJChoosePhoto>
				<SelectItem
					title={t('my.name')}
					info={username}
					placeholder={t('common.please_enter')}
					onPress={() => onClickItem(1)}
				/>
				<SelectItem
					title={t('my.phone')}
					info={mobile}
					placeholder={t('common.please_enter')}
					onPress={() => onClickItem(2)}
				/>
				<SelectItem
					title={t('my.email')}
					info={email}
					placeholder={t('common.please_enter')}
					onPress={() => onClickItem(3)}
				/>
				<SelectItem
					title={t('language')}
					info={t(`lan_${currentLan}`)}
					placeholder={t('common.please_select')}
					onPress={() => {
						console.log(currentLan);
						setOpenModal(true);
					}}
				/>
			</Box>
			<ZJComponent.BottomChooseList
				isOpen={openModal}
				list={languageArray}
				selectItem={currentLan == 'zh' ? t('lan_zh') : t('lan_en')}
				onClose={() => setOpenModal(false)}
				clickOKBtn={item => {
					changeLanguage(item);
				}}
			/>
		</ScrollView>
	);
}

/*个人资料-编辑文本*/
export function PersonalEditText({navigation, route}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		var type = route.params?.type;
		var name =
			type === 2 ? t('my.phone') : type === 3 ? t('my.email') : t('my.name');
		navigation.setOptions({
			headerTitle: name,
		});
		setTypeName(name);
		setInfo(route.params?.info);
	}, [navigation, route]);

	React.useEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<Button
					onPress={onClickNavRightBtn}
					variant="unstyled"
					_text={{
						color: mainColor,
						fontSize: pxToDp(16),
					}}>
					{t('common.complete')}
				</Button>
			),
		});
	});

	/*type的名称*/
	const [typeName, setTypeName] = React.useState('');

	/*编辑的内容*/
	const [info, setInfo] = React.useState('');

	const onClickNavRightBtn = React.useCallback(() => {
		Keyboard.dismiss();
		global.showLoading();
		var type = route.params?.type;
		var params =
			type === 2
				? {mobile: info}
				: type === 3
				? {email: info}
				: {nickname: info};
		UserNetwork.updateUser(params)
			.then(res => {
				global.hiddenLoading();
				if (route.params.callback) {
					route.params.callback(info);
				}
				navigation.goBack();
			})
			.catch(err => {
				console.log(err.data);
				global.hiddenLoading();
				global.showToast({title: err.msg});
			});
	}, [navigation, route, info]);

	return (
		<KeyboardAvoidingView flex={1}>
			<ScrollView bg="#F8F8F8" w="100%">
				<Box mx={pxToDp(16)} bg="#fff" borderRadius={pxToDp(4)} mt={pxToDp(20)}>
					<Input
						w={'100%'}
						h={pxToDp(60)}
						fontSize={pxToDp(14)}
						variant="unstyled"
						placeholderTextColor="#999999"
						placeholder={t('common.please_enter')}
						color="#333333"
						value={info}
						onChangeText={setInfo}
					/>
				</Box>
			</ScrollView>
		</KeyboardAvoidingView>
	);
}
