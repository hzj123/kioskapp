import React, {useState, useLayoutEffect} from 'react';
import {Box, Text, ScrollView, Image, Pressable} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
	OrderNetwork,
	ActivityNetwork,
} from '../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

import {DeviceEventEmitter} from 'react-native';

import FastImage from 'react-native-fast-image';

const MyItem = props => {
	return (
		<Pressable onPress={props.onPress}>
			<Box
				w="100%"
				h={pxToDp(54)}
				flexDir="row"
				alignItems="center"
				px={pxToDp(16)}>
				<Box size={pxToDp(20)} alignItems="center" alignItems="center">
					{props.icon}
				</Box>
				<Text ml={pxToDp(15)} fontSize={pxToDp(16)} color="#333">
					{props.title}
				</Text>
				{props.num ? (
					<Box h="100%" pt={pxToDp(10)} ml={pxToDp(-4)}>
						<Box
							h={pxToDp(16)}
							minW={pxToDp(16)}
							bg="#F53F3F"
							borderRadius={pxToDp(8)}
							px={pxToDp(4)}
							alignItems="center"
							justifyContent="center">
							<Text color="#fff" fontSize={pxToDp(11)}>
								{props.num}
							</Text>
						</Box>
					</Box>
				) : null}
				<Box flex={1} />
				<JFLIcon.right_arrow />
			</Box>
		</Pressable>
	);
};

export default function MyScreen({navigation}) {
	const {t} = useTranslation();

	//---获取未读消息---start----

	/*未读消息数*/
	var [unreadNum, setUnreadNum] = React.useState(0);

	React.useEffect(() => {
		const msgSubscription = DeviceEventEmitter.addListener('myMsgNum', msg => {
			setUnreadNum(msg ?? 0);
		});
		return function cleanup() {
			msgSubscription?.remove();
		};
	}, [navigation]);

	useFocusEffect(
		React.useCallback(() => {
			getAllNum();
		}, []),
	);

	/*获取所有的未读消息数*/
	const getAllNum = () => {
		Promise.all([
			UserNetwork.getMessagesCount(),
			OrderNetwork.getMessageNum(),
			ActivityNetwork.getMessageNum(),
		])
			.then(res => {
				DeviceEventEmitter.emit('myMsgNum', res[0].messagesCount);
				DeviceEventEmitter.emit('orderNum', res[1]);
				DeviceEventEmitter.emit('activityNum', res[2]);
			})
			.catch(err => {});
	};
	//---获取未读消息---end----

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('tab.tab_my'),
		});
	});

	useFocusEffect(
		React.useCallback(() => {
			setName(global.user?.nickname);
			setPhoto(global.user?.photo);
			setUserType(global.user?.userType);
		}, []),
	);

	var [userType, setUserType] = React.useState(global.user?.userType);

	var [photo, setPhoto] = React.useState(global.user?.photo);

	var [name, setName] = React.useState(global.user?.nickname);

	const onClickItem = index => {
		if (0 === index) {
			navigation.navigate('Personal');
		} else if (1 === index) {
			navigation.navigate('ChangePsw');
		} else if (2 === index) {
			navigation.navigate('ServiceList');
		} else if (3 === index) {
			navigation.navigate('MessageTabList');
		} else if (4 === index) {
			navigation.navigate('QuestionList');
		} else if (5 === index) {
			navigation.navigate('EmployeeList');
		} else if (6 === index) {
			setOpenCancellationModal(true);
		}
	};

	const [openModal, setOpenModal] = useState(false);

	const onClickLogutBtn = () => {
		setOpenModal(false);
		global.showLoading();
		storage
			.save({
				key: 'token',
				data: null,
			})
			.then(res => {
				global.hiddenLoading();
				global.user = null;
				global.token = null;
				navigation.push('Login');
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	const [openCancellationModal, setOpenCancellationModal] = useState(false);

	const onClickCancellationBtn = () => {
		setOpenCancellationModal(false);
		global.showLoading();
		UserNetwork.logOff()
			.then(res => {
				global.hiddenLoading();
				onClickLogutBtn();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({
					title: err.message,
				});
			});
	};

	return (
		<ScrollView w="100%" bg="#F8F8F8" showsVerticalScrollIndicator={false}>
			<Box flexDir="row" alignItems="center" mt={pxToDp(20)}>
				<ZJComponent.ZJImage
					mx={pxToDp(16)}
					size={pxToDp(56)}
					borderRadius={pxToDp(28)}
					uri={photo ?? ''}
				/>
				<Text fontSize={pxToDp(18)} color="#1D2129">
					{name ?? ''}
				</Text>
			</Box>
			<Box mx={pxToDp(16)} bg="#fff" borderRadius={pxToDp(8)} mt={pxToDp(38)}>
				<MyItem
					icon={<JFLIcon.my_personal />}
					title={t('my.personal_data')}
					onPress={() => onClickItem(0)}
				/>
				<MyItem
					icon={<JFLIcon.my_change_psw />}
					title={t('my.changePsw')}
					onPress={() => onClickItem(1)}
				/>
				<MyItem
					icon={<JFLIcon.my_contact_customer />}
					title={t('my.contact_customer_service')}
					onPress={() => onClickItem(2)}
				/>
				<MyItem
					icon={<JFLIcon.my_msg />}
					title={t('my.message')}
					num={unreadNum}
					onPress={() => onClickItem(3)}
				/>
				<MyItem
					icon={<JFLIcon.my_question />}
					title={t('my.Q&A')}
					onPress={() => onClickItem(4)}
				/>
				{userType === 0 ? (
					<MyItem
						icon={<JFLIcon.my_employee />}
						title={t('my.employee_data')}
						onPress={() => onClickItem(5)}
					/>
				) : null}

				<MyItem
					icon={<JFLIcon.my_personal />}
					title={t('my.account_cancellation')}
					onPress={() => onClickItem(6)}
				/>
			</Box>
			<ZJComponent.ThemeButton
				m={pxToDp(16)}
				mt={pxToDp(50)}
				title={t('common.Log_out')}
				onPress={() => setOpenModal(true)}
			/>
			<ZJComponent.InfoModal
				isOpen={openModal}
				title=""
				info={t('common.logout_confirm_info')}
				onClose={() => setOpenModal(false)}
				clickOKBtn={onClickLogutBtn}
			/>
			<ZJComponent.InfoModal
				isOpen={openCancellationModal}
				title=""
				info={t('common.cancellation_confirm_info')}
				onClose={() => setOpenCancellationModal(false)}
				clickOKBtn={onClickCancellationBtn}
			/>
		</ScrollView>
	);
}
