import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	Pressable,
	Input,
	ScrollView,
	KeyboardAvoidingView,
} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
} from '../../common/commonIndex.js';

import {RefreshControl} from 'react-native';

import {useTranslation} from 'react-i18next';

const Item = props => {
	const {t, i18n} = useTranslation();

	return (
		<Pressable onPress={() => {}}>
			<Box
				bg="#fff"
				mb={pxToDp(20)}
				mx={pxToDp(16)}
				py={pxToDp(20)}
				px={pxToDp(16)}
				borderRadius={pxToDp(4)}>
				<Text color="#333" fontSize={pxToDp(16)} fontWeight="500" noOfLines={1}>
					{/*{props.item?.department ?? ''}*/}
					{(i18n.language == 'zh'
						? props.item?.altDepartment
						: props.item?.department) ?? ''}
				</Text>
				<Box mt={pxToDp(12)} flexDir="row">
					<Box alignItems="center" justifyContent="center" w={pxToDp(24)}>
						<JFLIcon.my_email />
					</Box>
					<Text
						ml={pxToDp(4)}
						color="#666"
						fontSize={pxToDp(14)}
						fontWeight="400"
						noOfLines={1}>
						{t('my.email') + '：' + (props.item?.email ?? '')}
					</Text>
				</Box>
				<Box mt={pxToDp(12)} flexDir="row">
					<Box alignItems="center" justifyContent="center" w={pxToDp(24)}>
						<JFLIcon.my_contact_customer color="#666" />
					</Box>
					<Text
						ml={pxToDp(4)}
						color="#666"
						fontSize={pxToDp(14)}
						fontWeight="400"
						noOfLines={1}>
						{t('my.phone') + '：'}
						<Text
							ml={pxToDp(4)}
							color={mainColor}
							fontSize={pxToDp(14)}
							fontWeight="400"
							noOfLines={1}>
							{props.item?.mobile ?? ''}
						</Text>
					</Text>
				</Box>
			</Box>
		</Pressable>
	);
};

/*客服列表*/
export default function ServiceList({navigation}) {
	const {t} = useTranslation();

	const [data, setData] = React.useState([]);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.contact_customer_service'),
		});
	}, [navigation]);

	const [refreshing, setRefreshing] = React.useState(false);

	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const [page, setPage] = React.useState(1);

	const onRefresh = React.useCallback(() => {
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		setRefreshing(true);
		getInfo(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index);
		}
	}, [loadMore]);

	const getInfo = index => {
		UserNetwork.customerService()
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				s.canLoadMore = false;
				s.showLoadMoreCompoment = false;
				setData(newData);
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				global.showToast({title: err.message});
			});
	};

	React.useEffect(() => {
		setTimeout(() => {
			onRefresh();
		}, 400);
		return function cleanup() {};
	}, []);

	const renderItem = ({item, index}) => {
		return <Item item={item} />;
	};

	return (
		<FlatList
			flex={1}
			bg="#F8F8F8"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListHeaderComponent={<Box h={pxToDp(20)} />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}
