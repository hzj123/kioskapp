import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	Pressable,
	Input,
	ScrollView,
	KeyboardAvoidingView,
} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
} from '../../common/commonIndex.js';

import {Keyboard} from 'react-native';

import {useTranslation} from 'react-i18next';

/*Item的标题和内容*/
const ItemTitleInfo = props => {
	return (
		<Box flexDir="row" {...props}>
			<Text
				w={pxToDp(80)}
				color="#333"
				fontSize={pxToDp(14)}
				fontWeight="400"
				noOfLines={1}>
				{props.title ?? ''}
			</Text>
			<Text
				flex={1}
				color="#333"
				fontSize={pxToDp(14)}
				fontWeight="400"
				noOfLines={2}>
				{props.info ?? ''}
			</Text>
		</Box>
	);
};

const Item = props => {
	const {t} = useTranslation();
	const [openModal, setOpenModal] = useState(false);

	return (
		<>
			<Pressable onPress={() => {}}>
				<Box
					bg="#fff"
					my={pxToDp(10)}
					mx={pxToDp(16)}
					py={pxToDp(20)}
					px={pxToDp(16)}
					borderRadius={pxToDp(4)}>
					<Box flexDir="row" alignItems="center">
						<ItemTitleInfo
							flex={1}
							title={t('my.name') + ':'}
							info={props.item?.nickname ?? ''}
						/>
						<Button
							onPress={props.onPress}
							size={pxToDp(30)}
							variant="unstyled"
							startIcon={<JFLIcon.icon_edit />}
						/>
						<Button
							onPress={() => setOpenModal(true)}
							size={pxToDp(30)}
							variant="unstyled"
							startIcon={<JFLIcon.icon_delete />}
						/>
					</Box>
					<ItemTitleInfo
						mt={pxToDp(12)}
						title={t('my.username') + ':'}
						info={props.item?.username}
					/>
					<ItemTitleInfo
						mt={pxToDp(12)}
						title={t('my.password') + ':'}
						info={props.item?.password ?? ''}
					/>
					<ItemTitleInfo
						mt={pxToDp(12)}
						title={t('my.store') + ':'}
						info={(props.item?.shopList ?? [])
							.map(item => item.shopName)
							.join('，')}
					/>
				</Box>
			</Pressable>
			<ZJComponent.InfoModal
				isOpen={openModal}
				title=""
				info={t('common.delete_confirm_info')}
				onClose={() => setOpenModal(false)}
				clickOKBtn={() => {
					setOpenModal(false);
					props.deleteItem ? props.deleteItem(props.item?.userId) : null;
				}}
			/>
		</>
	);
};

/*员工资料*/
export default function EmployeeList({navigation}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.employee_data'),
			headerRight: () => (
				<Button
					onPress={() => onClickNavRightBtn()}
					variant="unstyled"
					_text={{
						color: mainColor,
						fontSize: pxToDp(16),
					}}>
					{t('common.add')}
				</Button>
			),
		});
	}, [navigation]);

	const onClickNavRightBtn = () => {
		navigation.navigate('EmployeeEdit', {
			callback: () => {
				setTimeout(() => {
					onRefresh();
				}, 400);
			},
		});
	};

	const renderItem = ({item, index}) => {
		return (
			<Item
				item={item}
				onPress={() => {
					navigation.navigate('EmployeeEdit', {
						model: item,
						callback: () => {
							setTimeout(() => {
								onRefresh();
							}, 400);
						},
					});
				}}
				deleteItem={i => {
					deleteItem(i);
				}}
			/>
		);
	};

	const deleteItem = id => {
		global.showLoading();
		UserNetwork.deleteEmployee(id)
			.then(res => {
				global.hiddenLoading();
				onRefresh();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index);
		}
	}, [loadMore]);

	const getInfo = index => {
		UserNetwork.list()
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	React.useEffect(() => {
		setTimeout(() => {
			onRefresh();
		}, 400);
		return function cleanup() {};
	}, [navigation]);

	//---数据获取相关---end----

	return (
		<FlatList
			flex={1}
			bg="#F8F8F8"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}

const EmployeeEditInputItem = props => {
	const {t} = useTranslation();
	return (
		<Box
			w="100%"
			px={pxToDp(16)}
			h={pxToDp(54)}
			alignItems="center"
			justifyContent="center"
			flexDir="row">
			{props.require === true ? (
				<Text
					fontSize={pxToDp(14)}
					color="#F53F3F"
					position="absolute"
					top={pxToDp(14)}
					left={pxToDp(12)}>
					*
				</Text>
			) : null}
			<Text w={pxToDp(90)} color="#333" fontSize={pxToDp(14)}>
				{props.title ?? ''}
			</Text>
			<Input
				flex={1}
				fontSize={pxToDp(16)}
				variant="unstyled"
				placeholderTextColor="#999999"
				color="#333333"
				placeholder={t('common.please_enter')}
				value={props.value}
				onChangeText={props.onChangeText}
			/>
		</Box>
	);
};

/*新增/编辑员工资料*/
export function EmployeeEdit({navigation, route}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: route?.params?.model ? t('common.edit') : t('common.add'),
		});
	}, [navigation, route]);

	React.useLayoutEffect(() => {
		let model = route.params?.model;
		setModel(model);
		setName(model?.nickname);
		setUsername(model?.username);
		setPassword(model?.password);
		setSelectStoreArray(model?.shopList ?? []);
		if (model?.store) {
			var array = model?.store.split(',');
			console.log(array);
			setSelectStoreArray(array);
		}
	}, [route.params?.model]);

	const [model, setModel] = React.useState('');

	const [name, setName] = React.useState('');
	const [username, setUsername] = React.useState('');
	const [password, setPassword] = React.useState('');
	const [phone, setPhone] = React.useState('');
	const [email, setEmail] = React.useState('');

	/*所有商店*/
	const [storeArray, setStoreArray] = React.useState([]);
	/*选中的商店的集合*/
	const [selectStoreArray, setSelectStoreArray] = React.useState([]);

	const _getAllShop = () => {
		global.showLoading();
		UserNetwork.getAllShop()
			.then(res => {
				global.hiddenLoading();
				setStoreArray(res ?? []);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	React.useLayoutEffect(() => {
		_getAllShop();
	}, [navigation]);

	/*修改/保存*/
	const onClickSaveBtn = () => {
		var params = {};
		if (model != null) {
			params.userId = model.userId;
		}
		params.nickname = name;
		params.username = username;
		params.password = password;
		params.phone = phone;
		params.email = email;

		var allShopIds = storeArray.map(item => item.shopId);
		params.shopList = selectStoreArray?.filter(
			item => allShopIds.indexOf(item.shopId) > -1,
		);
		Keyboard.dismiss();
		global.showLoading();
		UserNetwork.saveCustomerUserExtend(params)
			.then(res => {
				global.hiddenLoading();
				if (route.params?.callback) {
					route.params?.callback();
				}
				navigation.goBack();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	return (
		<KeyboardAvoidingView
			flex={1}
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
			<ScrollView>
				<Box
					alignItems="center"
					mt={pxToDp(20)}
					mx={pxToDp(16)}
					bg="#fff"
					overflow="hidden"
					borderRadius={pxToDp(10)}>
					<EmployeeEditInputItem
						require={true}
						title={t('my.name') + '：'}
						value={name}
						onChangeText={setName}
					/>
					<EmployeeEditInputItem
						require={true}
						title={t('my.username') + '：'}
						value={username}
						onChangeText={setUsername}
					/>
					<EmployeeEditInputItem
						require={true}
						title={t('my.password') + '：'}
						value={password}
						onChangeText={setPassword}
					/>
					<EmployeeEditInputItem
						title={t('my.phone') + '：'}
						value={phone}
						onChangeText={setPhone}
					/>
					<EmployeeEditInputItem
						title={t('my.email') + '：'}
						value={email}
						onChangeText={setEmail}
					/>
					<Box py={pxToDp(20)} px={pxToDp(16)} bg="#fff" flexDir="row">
						<Text
							fontSize={pxToDp(14)}
							color="#F53F3F"
							position="absolute"
							top={pxToDp(14)}
							left={pxToDp(12)}>
							*
						</Text>
						<Text fontSize={pxToDp(14)} w={pxToDp(90)} color="#333">
							{t('my.store')}
						</Text>
						<Box flex={1} ml={pxToDp(10)}>
							{storeArray.map((item, index) => {
								return (
									<ZJComponent.ZJCheckBox
										key={item + index}
										IsChecked={
											selectStoreArray
												.map(item => item.shopId)
												.indexOf(item.shopId) > -1
										}
										title={item.shopName}
										onPress={() => {
											var array = [...selectStoreArray];
											var index = selectStoreArray
												.map(item => item.shopId)
												.indexOf(item.shopId);
											if (index > -1) {
												array.splice(index, 1);
											} else {
												array.push(item);
											}
											setSelectStoreArray(array);
										}}
									/>
								);
							})}
						</Box>
					</Box>
				</Box>
				<ZJComponent.ThemeButton
					flex={1}
					mt={pxToDp(50)}
					mx={pxToDp(16)}
					h={pxToDp(42)}
					title={t('common.btn_submit')}
					isDisabled={
						!(
							name?.length > 0 &&
							username?.length > 0 &&
							password?.length > 0 &&
							selectStoreArray?.length > 0
						)
					}
					onPress={() => {
						onClickSaveBtn();
					}}
				/>
			</ScrollView>
		</KeyboardAvoidingView>
	);
}
