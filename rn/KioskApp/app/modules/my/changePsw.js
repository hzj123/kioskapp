import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Pressable,
	Input,
	ScrollView,
	KeyboardAvoidingView,
} from 'native-base';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
} from '../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

const Item = props => {
	const {t} = useTranslation();
	return (
		<Box
			w="100%"
			px={pxToDp(16)}
			h={pxToDp(54)}
			alignItems="center"
			justifyContent="center"
			flexDir="row">
			<Text w={pxToDp(100)} color="#333" fontSize={pxToDp(14)}>
				{props.title}
			</Text>
			<Input
				flex={1}
				fontSize={pxToDp(16)}
				variant="unstyled"
				placeholderTextColor="#999999"
				color="#333333"
				placeholder={t('common.please_enter')}
				value={props.value}
				onChangeText={props.onChangeText}
			/>
		</Box>
	);
};

/*修改密码*/
export default function ChangePsw({navigation, route}) {
	const {t} = useTranslation();

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('my.changePsw'),
		});
	});

	const [name, setName] = React.useState('');
	const [link, setLink] = React.useState('');

	const [oldPsw, setOldPsw] = React.useState('');
	const [newPsw, setNewPsw] = React.useState('');
	const [confirmPsw, setConfirmPsw] = React.useState('');

	const onClickBottomBtn = () => {
		global.showLoading();
		UserNetwork.changePassword(oldPsw, newPsw)
			.then(res => {
				global.hiddenLoading();
				navigation.goBack();
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};
	return (
		<KeyboardAvoidingView
			flex={1}
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
			<ScrollView>
				<Box
					alignItems="center"
					mt={pxToDp(20)}
					mx={pxToDp(16)}
					bg="#fff"
					borderRadius={pxToDp(10)}>
					<Item
						title={t('my.Old_Password')}
						value={oldPsw}
						onChangeText={setOldPsw}
					/>
					<Item
						title={t('my.New_Password')}
						value={newPsw}
						onChangeText={setNewPsw}
					/>
					<Item
						title={t('my.Confirm_New_Password')}
						value={confirmPsw}
						onChangeText={setConfirmPsw}
					/>
				</Box>
				<ZJComponent.ThemeButton
					flex={1}
					mt={pxToDp(50)}
					mx={pxToDp(16)}
					h={pxToDp(42)}
					title={t('common.btn_confirm')}
					isDisabled={
						!(oldPsw.length > 0 && newPsw.length === 6 && confirmPsw == newPsw)
					}
					onPress={onClickBottomBtn}
				/>
			</ScrollView>
		</KeyboardAvoidingView>
	);
}
