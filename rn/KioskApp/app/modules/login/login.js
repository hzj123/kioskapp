import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	Image,
	Input,
	Pressable,
	ScrollView,
	KeyboardAvoidingView,
	StatusBar,
} from 'native-base';
import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	pxToDp,
	ZJComponent,
	mainColor,
	storage,
	LoginNetwork,
	UserNetwork,
} from '../../common/commonIndex.js';

import {Dimensions} from 'react-native';
// 获取竖屏模式的高度
const deviceHeight = Dimensions.get('window').height;

import {useTranslation} from 'react-i18next';
import {Keyboard} from 'react-native';

export function Login({navigation, route}) {
	const {t} = useTranslation();

	const [name, setName] = useState('test1');
	const [psw, setPsw] = useState('123456');

	useFocusEffect(
		React.useCallback(() => {
			setName('');
			setPsw('');
			return () => {};
		}, [navigation, route]),
	);

	const onClickLoginBtn = () => {
		Keyboard.dismiss();
		global.showLoading();
		LoginNetwork.login(name, psw)
			.then(res => {
				global.hiddenLoading();
				global.user = res;
				storage.save({
					key: 'token',
					data: res.token,
				});
				global.token = res.token;
				navigation.push('Tab');
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({
					title: err.message,
					placement: 'bottom',
					// status: 'warning',
				});
			});
	};

	const onClickToRegister = () => {
		navigation.navigate('RegisterPage');
	};

	return (
		<Box
			style={{
				flex: 1,
				backgroundColor: '#F8F8F8',
			}}>
			<StatusBar hidden={true} />
			<Image
				position="absolute"
				top={0}
				width={'100%'}
				height={pxToDp(240)}
				alt=""
				resizeMode={'cover'}
				source={require('../../resource/login_top_bg.png')}
			/>
			<KeyboardAvoidingView
				flex={1}
				behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
				<ScrollView>
					<Box h={deviceHeight}>
						<Box flex={1} justifyContent="center">
							<Box
								alignItems="center"
								py={pxToDp(55)}
								px={pxToDp(20)}
								mx={pxToDp(16)}
								bg="#fff"
								borderRadius={20}>
								<Text fontSize={pxToDp(22)}>{t('login.title')}</Text>
								<Input
									w={'100%'}
									h={pxToDp(50)}
									mt={pxToDp(70)}
									fontSize={pxToDp(14)}
									variant="unstyled"
									placeholderTextColor="#999999"
									placeholder={t('placeholder.login_name')}
									color="#333333"
									InputLeftElement={
										<Button
											variant="unstyled"
											size={pxToDp(20)}
											startIcon={<JFLIcon.login_name />}
										/>
									}
									value={name}
									onChangeText={setName}
								/>
								<Box w={'100%'} h={'1px'} bg="#F8F8F8" />
								<Input
									w={'100%'}
									h={pxToDp(50)}
									fontSize={pxToDp(14)}
									variant="unstyled"
									placeholderTextColor="#999999"
									placeholder={t('placeholder.login_psw')}
									color="#333333"
									type="password"
									InputLeftElement={
										<Button
											variant="unstyled"
											size={pxToDp(20)}
											startIcon={<JFLIcon.login_psw />}
										/>
									}
									value={psw}
									onChangeText={setPsw}
								/>
								<ZJComponent.ThemeButton
									w={'100%'}
									mt={pxToDp(60)}
									h={pxToDp(42)}
									title={t('login.btn_login')}
									isDisabled={!(name.length > 0 && psw.length > 0)}
									onPress={onClickLoginBtn}
								/>
								<Box w={'100%'} alignItems="flex-end">
									<ZJComponent.ThemeButton
										mt={pxToDp(10)}
										h={pxToDp(30)}
										py={0}
										title={t('login.btn_register')}
										bg={'#fff'}
										color={mainColor}
										onPress={onClickToRegister}
									/>
								</Box>
							</Box>
						</Box>
						<Box
							mx={pxToDp(16)}
							my={pxToDp(20)}
							alignItems="center"
							justifyContent="center"
							zIndex={9}>
							<Text color="#999" fontSize={pxToDp(12)}>
								{t('login.bottom_tip_1')}
								<Text
									color={mainColor}
									onPress={() => {
										navigation.navigate('WebDetail', {
											title: t('login.privacy'),
											url: 'http://3.101.56.185:8870/policy',
										});
									}}>
									{t('login.privacy')}
								</Text>
								{t('login.bottom_tip_2')}
								<Text
									color={mainColor}
									onPress={() => {
										navigation.navigate('WebDetail', {
											title: t('login.terms'),
											url: 'http://3.101.56.185:8870/service',
										});
									}}>
									{t('login.terms')}
								</Text>
							</Text>
						</Box>
					</Box>
				</ScrollView>
			</KeyboardAvoidingView>
		</Box>
	);
}

/*注册页面*/
export function RegisterPage({navigation, route}) {
	const {t} = useTranslation();

	const [name, setName] = useState('');
	const [psw, setPsw] = useState('');
	const [pswConfrim, setPswConfrim] = useState('');

	useFocusEffect(
		React.useCallback(() => {
			setName('');
			setPsw('');
			setPswConfrim('');
			return () => {};
		}, [navigation, route]),
	);

	/*注册*/
	const onClickRegisterBtn = () => {
		global.showLoading();
		UserNetwork.register(name, psw)
			.then(res => {
				global.hiddenLoading();
				navigation.navigate('Login');
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({
					title: err.message,
					placement: 'bottom',
					// status: 'warning',
				});
			});
	};

	return (
		<Box
			style={{
				flex: 1,
				backgroundColor: '#F8F8F8',
			}}>
			<Image
				position="absolute"
				top={0}
				width={'100%'}
				height={pxToDp(240)}
				alt=""
				source={{
					uri: 'https://img1.baidu.com/it/u=3009731526,373851691&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1664298000&t=1d9c2c16498f39618bad58b0c17343c7',
				}}
			/>
			<KeyboardAvoidingView
				flex={1}
				behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
				<ScrollView>
					<Box h={deviceHeight} justifyContent="center">
						<Box
							alignItems="center"
							py={pxToDp(55)}
							px={pxToDp(20)}
							mx={pxToDp(16)}
							bg="#fff"
							borderRadius={20}>
							<Text fontSize={pxToDp(22)}>{t('login.register_title')}</Text>
							<Input
								w={'100%'}
								h={pxToDp(50)}
								mt={pxToDp(70)}
								fontSize={pxToDp(14)}
								variant="unstyled"
								placeholderTextColor="#999999"
								placeholder={t('placeholder.login_name')}
								color="#333333"
								InputLeftElement={
									<Button
										variant="unstyled"
										size={pxToDp(20)}
										startIcon={<JFLIcon.login_name />}
									/>
								}
								value={name}
								onChangeText={setName}
							/>
							<Box w={'100%'} h={'1px'} bg="#F8F8F8" />
							<Input
								w={'100%'}
								h={pxToDp(50)}
								fontSize={pxToDp(14)}
								variant="unstyled"
								placeholderTextColor="#999999"
								placeholder={t('placeholder.login_psw')}
								color="#333333"
								type="password"
								InputLeftElement={
									<Button
										variant="unstyled"
										size={pxToDp(20)}
										startIcon={<JFLIcon.login_psw />}
									/>
								}
								value={psw}
								onChangeText={setPsw}
							/>
							<Box w={'100%'} h={'1px'} bg="#F8F8F8" />
							<Input
								w={'100%'}
								h={pxToDp(50)}
								fontSize={pxToDp(14)}
								variant="unstyled"
								placeholderTextColor="#999999"
								placeholder={t('placeholder.login_pswconfrim')}
								color="#333333"
								type="password"
								InputLeftElement={
									<Button
										variant="unstyled"
										size={pxToDp(20)}
										startIcon={<JFLIcon.login_psw />}
									/>
								}
								value={pswConfrim}
								onChangeText={setPswConfrim}
							/>
							<ZJComponent.ThemeButton
								w={'100%'}
								mt={pxToDp(60)}
								h={pxToDp(42)}
								title={t('login.btn_register')}
								isDisabled={
									!(name.length > 0 && psw.length > 0 && psw == pswConfrim)
								}
								onPress={onClickRegisterBtn}
							/>
						</Box>
					</Box>
				</ScrollView>
			</KeyboardAvoidingView>
		</Box>
	);
}
