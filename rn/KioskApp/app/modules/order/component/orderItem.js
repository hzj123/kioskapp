import * as React from 'react';
import {Box, Text, Button, Pressable} from 'native-base';

import {JFLIcon, pxToDp} from '../../../common/commonIndex.js';

import {useTranslation} from 'react-i18next';

/*
 * 订单item
 * item.status	0-未处理 1-处理中 2-已处理
 * item.urgency	0-非常紧急 1-中度紧急 2-一般
 */
export default function OrderItem(argument) {
	/*订单状态*/
	const TypeItem = props => {
		const {t} = useTranslation();
		var title = 'order.state_wait',
			color = '#F53F3F';
		switch (props.status) {
			case 1:
				title = 'order.state_doing';
				color = '#165DFF';
				break;
			case 2:
				title = 'order.state_completed';
				color = '#999999';
				break;
		}
		return (
			<Box
				alignItems="center"
				justifyContent="center"
				h={pxToDp(30)}
				px={pxToDp(16)}
				borderColor={color}
				borderRadius={pxToDp(2)}
				borderWidth={pxToDp(1)}
				{...props}>
				<Text color={color} fontSize={pxToDp(15)} {...props}>
					{t(title)}
				</Text>
			</Box>
		);
	};
	const [hotNum, setHotNum] = React.useState(0);

	React.useEffect(() => {
		setHotNum(argument.item?.notReadNumber);
	}, [argument.item?.notReadNumber]);

	return (
		<Pressable
			onPress={
				argument.onPress != null
					? () => {
							if (argument.item != null) {
								argument.item.notReadNumber = 0;
							}
							setHotNum(0);
							argument.onPress();
					  }
					: null
			}>
			<Box
				px={pxToDp(17)}
				py={pxToDp(20)}
				mx={pxToDp(16)}
				my={pxToDp(10)}
				borderRadius={pxToDp(4)}
				bg="#fff">
				{hotNum > 0 ? (
					<Box
						bg="#F53F3F"
						minW={pxToDp(16)}
						h={pxToDp(16)}
						borderRadius={pxToDp(8)}
						alignItems="center"
						justifyContent="center"
						zIndex={9}
						position="absolute"
						px={pxToDp(4)}
						top={pxToDp(12)}
						left={pxToDp(8)}>
						<Text fontSize={pxToDp(11)} color="#fff">
							{hotNum ?? ''}
						</Text>
					</Box>
				) : null}

				<Box display="flex" flexDir="row" alignItems="flex-start">
					<Box flex={1}>
						<Text color="#333333" fontSize={pxToDp(14)} noOfLines={1}>
							{argument.item?.title ?? ''}
						</Text>
						<Text color="#999999" fontSize={pxToDp(12)} noOfLines={1}>
							{argument.item?.createTime ?? ''}
						</Text>
					</Box>
					<TypeItem status={argument?.item?.status} ml={pxToDp(2)} />
				</Box>
				<Text color="#333333" fontSize={pxToDp(14)} noOfLines={2}>
					{argument.item?.content ?? ''}
				</Text>
				{argument.item?.firstTicketComm ? (
					<>
						<Box w="100%" h="1px" bg="#F8F8F8" my={pxToDp(12)} />
						<Text color="#666666" fontSize={pxToDp(14)} noOfLines={2}>
							{argument.item?.firstTicketComm?.content}
						</Text>
						<Text color="#999999" fontSize={pxToDp(12)} noOfLines={1}>
							{argument.item?.firstTicketComm?.createTime}
						</Text>
					</>
				) : null}
			</Box>
		</Pressable>
	);
}
