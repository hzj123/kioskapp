import * as React from 'react';
import {Box, Text, Button, Pressable, Image} from 'native-base';

import {
	JFLIcon,
	pxToDp,
	mainColor,
	ZJComponent,
} from '../../../common/commonIndex.js';

export function OrderDetailInfo(props) {
	return (
		<Box flexDir="row" pt={pxToDp(13)}>
			<Text fontSize={pxToDp(14)} color="#666" w={pxToDp(90)}>
				{props.title}
			</Text>
			<Text fontSize={pxToDp(14)} color="#333" flex={1}>
				{props.info}
			</Text>
		</Box>
	);
}

/*对话-左侧*/
export function OrderDetailLeftItem(props) {
	return (
		<Box
			flexDir="row"
			py={pxToDp(15)}
			pl={pxToDp(16)}
			pr={pxToDp(66)}
			mx={pxToDp(16)}
			justifyContent="flex-start"
			bg="#fff">
			<Box
				size={pxToDp(40)}
				borderRadius={pxToDp(4)}
				bg={mainColor}
				alignItems="center"
				justifyContent="center">
				{props.model?.customer != null ? (
					<ZJComponent.ZJImage
						size={pxToDp(40)}
						borderRadius={pxToDp(4)}
						uri={props.model?.customer?.photo}
					/>
				) : (
					<JFLIcon.service_photo />
				)}
			</Box>
			<Box flex={1} ml={pxToDp(10)} alignItems="flex-start">
				<Text fontSize={pxToDp(12)} color="#999" lineHeight={pxToDp(17)}>
					{props.model?.customer?.nickname ?? ''}{' '}
					{props.model?.createTime ?? ''}
				</Text>
				{/*{props.model.contentPic ? (
					<ZJComponent.ZJImage
						mt={pxToDp(3)}
						w={pxToDp(143)}
						h={pxToDp(110)}
						borderRadius={pxToDp(4)}
						uri={props.model?.contentPic}
					/>
				) : (
					<Box mt={pxToDp(3)} bg="#f2f2f2" borderRadius={pxToDp(4)}>
						<Text
							fontSize={pxToDp(14)}
							color="#333"
							px={pxToDp(12)}
							py={pxToDp(10)}>
							{props.model.content ?? ''}
						</Text>
					</Box>
				)}*/}
				{props.model.contentPic ? (
					<ZJComponent.ZJImage
						mt={pxToDp(3)}
						w={pxToDp(143)}
						h={pxToDp(110)}
						borderRadius={pxToDp(4)}
						uri={props.model?.contentPic}
					/>
				) : null}
				{props.model.content ? (
					<Box mt={pxToDp(3)} bg="#f2f2f2" borderRadius={pxToDp(4)}>
						<Text
							fontSize={pxToDp(14)}
							color="#333"
							px={pxToDp(12)}
							py={pxToDp(10)}>
							{props.model.content ?? ''}
						</Text>
					</Box>
				) : null}
			</Box>
		</Box>
	);
}

/*对话-右侧*/
export function OrderDetailRightItem(props) {
	return (
		<Box
			flexDir="row"
			py={pxToDp(15)}
			pr={pxToDp(16)}
			pl={pxToDp(66)}
			mx={pxToDp(16)}
			justifyContent="flex-end"
			bg="#fff">
			<Box flex={1} mr={pxToDp(10)} alignItems="flex-end">
				<Text
					fontSize={pxToDp(12)}
					color="#999"
					lineHeight={pxToDp(17)}
					textAlign="right">
					{props.model?.createTime ?? ''}{' '}
					{props.model?.customer?.nickname ?? ''}
				</Text>
				{/*{props.model.contentPic ? (
					<ZJComponent.ZJImage
						mt={pxToDp(3)}
						w={pxToDp(143)}
						h={pxToDp(110)}
						borderRadius={pxToDp(4)}
						uri={props.model?.contentPic}
					/>
				) : (
					<Box mt={pxToDp(3)} bg="#82C91E" borderRadius={pxToDp(4)}>
						<Text
							fontSize={pxToDp(14)}
							color="#fff"
							px={pxToDp(12)}
							py={pxToDp(10)}>
							{props.model.content ?? ''}
						</Text>
					</Box>
				)}*/}
				{props.model.contentPic ? (
					<ZJComponent.ZJImage
						mt={pxToDp(3)}
						w={pxToDp(143)}
						h={pxToDp(110)}
						borderRadius={pxToDp(4)}
						uri={props.model?.contentPic}
					/>
				) : null}
				{props.model.content ? (
					<Box mt={pxToDp(3)} bg="#82C91E" borderRadius={pxToDp(4)}>
						<Text
							fontSize={pxToDp(14)}
							color="#fff"
							px={pxToDp(12)}
							py={pxToDp(10)}>
							{props.model.content ?? ''}
						</Text>
					</Box>
				) : null}
			</Box>
			<ZJComponent.ZJImage
				size={pxToDp(40)}
				borderRadius={pxToDp(4)}
				uri={props.model?.customer?.photo}
			/>
		</Box>
	);
}
