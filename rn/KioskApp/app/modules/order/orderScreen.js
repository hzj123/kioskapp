import React, {useState, useLayoutEffect} from 'react';
import {Box, Text, Button, FlatList} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	UserNetwork,
	OrderNetwork,
	ActivityNetwork,
} from '../../common/commonIndex.js';

import OrderItem from './component/orderItem.js';

import {useTranslation} from 'react-i18next';

import {DeviceEventEmitter} from 'react-native';

export default function OrderScreen({navigation}) {
	const {t} = useTranslation();

	//---获取未读消息---start----
	useFocusEffect(
		React.useCallback(() => {
			getAllNum();
		}, []),
	);

	/*获取所有的未读消息数*/
	const getAllNum = () => {
		Promise.all([
			UserNetwork.getMessagesCount(),
			OrderNetwork.getMessageNum(),
			ActivityNetwork.getMessageNum(),
		])
			.then(res => {
				DeviceEventEmitter.emit('myMsgNum', res[0].messagesCount);
				DeviceEventEmitter.emit('orderNum', res[1]);
				DeviceEventEmitter.emit('activityNum', res[2]);
			})
			.catch(err => {});
	};
	//---获取未读消息---end----

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('tab.tab_order'),
			headerRight: () => (
				<Button
					onPress={() => onClickNavRightBtn()}
					variant="unstyled"
					_text={{
						color: mainColor,
						fontSize: pxToDp(16),
					}}>
					{t('common.add')}
				</Button>
			),
		});
	});

	const onClickNavRightBtn = () => {
		navigation.navigate('CreateOrder', {
			callback: () => {
				setTimeout(() => {
					onRefresh();
				}, 400);
			},
		});
	};

	React.useLayoutEffect(() => {
		setTimeout(() => {
			onRefresh();
		}, 400);
	}, []);

	const renderItem = ({item, index}) => {
		return (
			<OrderItem
				item={item}
				onPress={() => {
					var newData = [...data];
					newData[index].notReadNumber = 0;
					setData(newData);
					navigation.navigate('OrderDetail', {
						id: item.ticketId,
					});
				}}
			/>
		);
	};

	//---数据获取相关---starty----

	const [data, setData] = React.useState([]);
	const [page, setPage] = React.useState(1);

	const [refreshing, setRefreshing] = React.useState(false);
	const [loadMore, setLoadMore] = React.useState({
		showLoadMoreCompoment: false,
		canLoadMore: true,
		loading: false,
	});

	const onRefresh = React.useCallback(() => {
		setRefreshing(true);
		setTimeout(() => {
			setLoadMore({...loadMore, showLoadMoreCompoment: false});
		}, 250);
		setPage(1);
		getInfo(1);
	}, [page]);

	const onEndReached = React.useCallback(() => {
		if (refreshing === false && loadMore.canLoadMore && !loadMore.loading) {
			setLoadMore({...loadMore, loading: true});
			var index = page + 1;
			setPage(index);
			getInfo(index);
		}
	}, [loadMore, refreshing]);

	const getInfo = index => {
		OrderNetwork.findByParam(index)
			.then(res => {
				setRefreshing(false);
				var newData = res ?? [];
				var s = loadMore;
				console.log(newData);
				if (index === 1) {
					s.showLoadMoreCompoment = newData.length > 0;
					setData(newData);
				} else {
					setData([...data, ...newData]);
				}
				s.loading = false;
				s.canLoadMore = newData.length == 10;
				setLoadMore(s);
			})
			.catch(err => {
				setRefreshing(false);
				setLoadMore({...loadMore, loading: false});
				global.showToast({title: err.message});
			});
	};

	//---数据获取相关---end----

	return (
		<FlatList
			flex={1}
			bg="#F8F8F8"
			showsVerticalScrollIndicator={false}
			keyboardDismissMode="on-drag"
			horizontal={false}
			data={data}
			// mt="16px"
			renderItem={renderItem}
			keyExtractor={(item, index) => item + index}
			ListEmptyComponent={<ZJComponent.EmptyView />}
			ListFooterComponent={
				<ZJComponent.ListLoadMore
					show={loadMore.showLoadMoreCompoment}
					nomore={!loadMore.canLoadMore}
					loading={loadMore.loading}
				/>
			}
			onEndReachedThreshold={0.3}
			onEndReached={onEndReached}
			refreshControl={
				<ZJComponent.ListRefresh
					refreshing={refreshing}
					onRefresh={() => {
						onRefresh();
					}}
				/>
			}
		/>
	);
}
