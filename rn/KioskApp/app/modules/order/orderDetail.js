import React, {useState, useLayoutEffect} from 'react';
import {
	Box,
	Text,
	Button,
	FlatList,
	Input,
	Image,
	AspectRatio,
} from 'native-base';

import {useFocusEffect} from '@react-navigation/native';

import {
	JFLIcon,
	mainColor,
	pxToDp,
	ZJComponent,
	OrderNetwork,
	FileNetwork,
	ZJChoosePhoto,
} from '../../common/commonIndex.js';

import OrderItem from './component/orderItem.js';

import {
	OrderDetailInfo,
	OrderDetailLeftItem,
	OrderDetailRightItem,
} from './component/orderDetailComponent.js';
import {useTranslation} from 'react-i18next';

/*工单详情*/
export default function OrderDetail({navigation, route}) {
	const {t, i18n} = useTranslation();
	const [data, setData] = React.useState([]);

	const [model, setModel] = React.useState({});

	/*订单图片*/
	const [imgs, setImgs] = React.useState([]);

	React.useLayoutEffect(() => {
		setImgs(model?.contentPicsList ?? []);
		setHiddenBottomInput(!(model?.status === 0 || model?.status === 1));
	}, [model]);

	/*是否隐藏底部输入框*/
	const [hiddenBottomInput, setHiddenBottomInput] = React.useState(true);

	/*输入框中的内容*/
	const [inputInfo, setInputInfo] = React.useState('');

	const _listRef = React.useRef(null);

	React.useLayoutEffect(() => {
		navigation.setOptions({
			headerTitle: t('tab.tab_order'),
		});
	}, [navigation]);

	React.useLayoutEffect(() => {
		getInfo(route.params?.id);
	}, [route.params?.id]);

	const getInfo = id => {
		global.showLoading();
		Promise.all([OrderNetwork.detail(id), OrderNetwork.queryComm(id)])
			.then(res => {
				global.hiddenLoading();
				setModel(res[0]);
				setData(res[1]);
				setTimeout(() => {
					_listRef?.current?.scrollToEnd();
				}, 1000);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({
					title: err.message,
				});
			});
	};

	/*获取订单状态*/
	const getOrdetStatus = status => {
		switch (status) {
			case 0:
				return t('order.state_wait');
				break;
			case 1:
				return t('order.state_doing');
				break;
			case 2:
				return t('order.state_completed');
				break;
			default:
				return '';
		}
	};

	/*获取订单紧急程度*/
	const getOrdetUrgency = status => {
		switch (status) {
			case 0:
				return t('order.urgency_very');
				break;
			case 1:
				return t('order.urgency_moderate');
				break;
			case 2:
				return t('order.urgency_general');
				break;
			default:
				return '';
				break;
		}
	};

	const ListHeaderComponent = () => {
		var _this = this;
		return (
			<Box
				mx={pxToDp(16)}
				mt={pxToDp(20)}
				mb={pxToDp(10)}
				bg="#fff"
				borderRadius={pxToDp(4)}
				px={pxToDp(16)}
				pt={pxToDp(7)}
				pb={pxToDp(20)}>
				<OrderDetailInfo
					title={t('order.store') + ':'}
					info={(model?.shops?.map(item => item.shopName ?? '') ?? []).join(
						',',
					)}
				/>
				<OrderDetailInfo
					title={t('order.category') + ':'}
					info={
						(i18n.language == 'zh' ? model?.altTypeName : model?.typeName) ?? ''
					}
				/>
				<OrderDetailInfo
					title={t('order.state') + ':'}
					info={t(getOrdetStatus(model?.status))}
				/>
				<OrderDetailInfo
					title={t('order.emergency_degree') + ':'}
					info={getOrdetUrgency(model?.urgency)}
				/>
				<OrderDetailInfo
					title={t('order.create_time') + ':'}
					info={model?.createTime ?? ''}
				/>
				<OrderDetailInfo
					title={t('order.title') + ':'}
					info={model?.title ?? ''}
				/>
				<OrderDetailInfo
					title={t('order.content') + ':'}
					info={model?.content ?? ''}
				/>
				{imgs && imgs.length > 0 ? (
					imgs.length > 1 ? (
						<Box mt={pxToDp(11)} flexWrap="wrap" w="100%" flexDir="row">
							{imgs.map((item, index) => {
								return (
									<Box w="33%" p={pxToDp(4)} key={item + index}>
										<AspectRatio ratio={{base: 1, md: 1}} flex={1}>
											<>
												<ZJComponent.ZJImage
													key={item + index}
													size={pxToDp(92)}
													mt={pxToDp(11)}
													borderRadius={pxToDp(4)}
													uri={item}
												/>
											</>
										</AspectRatio>
									</Box>
								);
							})}
						</Box>
					) : (
						<ZJComponent.ZJImage
							mt={pxToDp(13)}
							w="100%"
							h={pxToDp(147)}
							borderRadius={pxToDp(4)}
							uri={imgs[0]}
						/>
					)
				) : null}
			</Box>
		);
	};

	const ListFooterComponent = () => {
		return <Box h={pxToDp(10)} />;
	};

	const renderItem = ({item, index}) => {
		return item?.userId == global.user?.userId ? (
			<OrderDetailRightItem model={item} />
		) : (
			<OrderDetailLeftItem model={item} />
		);
	};

	/*发送文本*/
	const sendMsg = info => {
		if (info?.length > 0) {
			_addComm({
				content: info,
				contentPic: null,
				ticketId: route.params?.id,
				userId: global.user?.userId,
			});
		} else {
		}
	};

	/*获取图片，并发送*/
	const getImage = img => {
		global.showLoading();
		FileNetwork.fileUpload(img)
			.then(res => {
				global.hiddenLoading();
				_addComm({
					content: null,
					contentPic: res.fileUrl,
					ticketId: route.params?.id,
					userId: global.user?.userId,
				});
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	function format(time, fmt) {
		var o = {
			'M+': time.getMonth() + 1, //月份
			'd+': time.getDate(), //日
			'H+': time.getHours(), //小时
			'm+': time.getMinutes(), //分
			's+': time.getSeconds(), //秒
			'q+': Math.floor((time.getMonth() + 3) / 3), //季度
			S: time.getMilliseconds(), //毫秒
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(
				RegExp.$1,
				(time.getFullYear() + '').substr(4 - RegExp.$1.length),
			);
		for (var k in o)
			if (new RegExp('(' + k + ')').test(fmt))
				fmt = fmt.replace(
					RegExp.$1,
					RegExp.$1.length == 1
						? o[k]
						: ('00' + o[k]).substr(('' + o[k]).length),
				);
		return fmt;
	}

	/*发送对话*/
	const _addComm = params => {
		global.showLoading();
		OrderNetwork.addComm(params)
			.then(res => {
				global.hiddenLoading();
				setInputInfo('');
				setData([
					{
						...params,
						createTime: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
						customer: global.user,
					},
					...data,
				]);
			})
			.catch(err => {
				global.hiddenLoading();
				global.showToast({title: err.message});
			});
	};

	return (
		<Box flex={1} bg="#F8F8F8">
			<FlatList
				ref={_listRef}
				flex={1}
				showsVerticalScrollIndicator={false}
				keyboardDismissMode="on-drag"
				horizontal={false}
				data={data}
				renderItem={renderItem}
				keyExtractor={(item, index) => item + index}
				ListHeaderComponent={ListHeaderComponent}
				ListFooterComponent={ListFooterComponent}
				ListEmptyComponent={<ZJComponent.EmptyView pt="20%" />}
				// onEndReachedThreshold={0.1}
				// onEndReached={() => {
				// 	if (!loadMore) {
				// 		setLoadMore(true);
				// 		setTimeout(() => {
				// 			setLoadMore(false);
				// 			console.log('fasdfas');
				// 		}, 2000);
				// 	}
				// }}
				// refreshControl={
				// 	<RefreshControl
				// 		refreshing={refreshing}
				// 		onRefresh={() => onRefresh(keyword, selectedIndex)}
				// 		title={'loading...'}
				// 	/>
				// }
			/>
			{hiddenBottomInput ? null : (
				<Box
					h={pxToDp(80)}
					b={0}
					px={pxToDp(32)}
					py={pxToDp(12)}
					bg="#fff"
					flexDir="row">
					<Box bg="#f4f4f4" flex={1} h={pxToDp(40)} borderRadius={pxToDp(4)}>
						<Input
							w={'100%'}
							h={'100%'}
							fontSize={pxToDp(14)}
							variant="unstyled"
							placeholderTextColor="#999999"
							placeholder={t('placeholder.order_input_content')}
							color="#333333"
							value={inputInfo}
							onChangeText={setInputInfo}
							returnKeyType="send"
							onSubmitEditing={e => {
								// console.log(e.nativeEvent);
								sendMsg(e?.nativeEvent?.text);
							}}
						/>
					</Box>
					<ZJChoosePhoto ml={pxToDp(12)} getImage={getImage}>
						<Box size={pxToDp(40)} alignItems="center" justifyContent="center">
							<JFLIcon.add_img />
						</Box>
					</ZJChoosePhoto>
				</Box>
			)}
		</Box>
	);
}
