import React, {useState, useLayoutEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Text, Box, Button, StatusBar} from 'native-base';

// import * as JFLIcon from '../../common/component/JFLIcon.js';

import {JFLIcon, mainColor} from '../../common/commonIndex.js';
import HomeScreen from '../home/homeScreen.js';
import OrderScreen from '../order/orderScreen.js';
import ActivityScreen from '../activity/activityScreen.js';
import MyScreen from '../my/myScreen.js';

import {useTranslation} from 'react-i18next';

import {DeviceEventEmitter} from 'react-native';

// //详情页面
// function MyScreen({navigation}) {
// 	const {t, i18n} = useTranslation();
// 	return (
// 		<Box style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
// 			<Button
// 				onPress={() => {
// 					navigation.navigate('Login');
// 					// i18n.changeLanguage('zh');
// 				}}>
// 				<Text>Login Screen</Text>
// 			</Button>
// 		</Box>
// 	);
// }

// //详情页面
// function DetailsScreen() {
// 	return (
// 		<Box style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
// 			<Text>Details!</Text>
// 		</Box>
// 	);
// }

//底部tabbar相关
const Tab = createBottomTabNavigator();

export default function TabScreen(props) {
	const {t, i18n} = useTranslation();

	/*未读消息数*/
	var [unreadNum1, setUnreadNum1] = React.useState(0);
	var [unreadNum2, setUnreadNum2] = React.useState(0);
	var [unreadNum3, setUnreadNum3] = React.useState(0);

	//---获取未读消息---start----
	React.useEffect(() => {
		const msgSubscription1 = DeviceEventEmitter.addListener('orderNum', msg => {
			console.log('get orderNum');
			setUnreadNum1(msg ?? 0);
		});
		const msgSubscription2 = DeviceEventEmitter.addListener(
			'activityNum',
			msg => {
				console.log('get activityNum');
				setUnreadNum2(msg ?? 0);
			},
		);
		const msgSubscription3 = DeviceEventEmitter.addListener('myMsgNum', msg => {
			console.log('get myMsgNum');
			setUnreadNum3(msg ?? 0);
		});
		return function cleanup() {
			msgSubscription1?.remove();
			msgSubscription2?.remove();
			msgSubscription3?.remove();
		};
	}, []);

	return (
		<>
			<StatusBar
				backgroundColor="#F8F8F8"
				barStyle="dark-content"
				hidden={false}
			/>
			<Tab.Navigator
				initialRouteName="首页"
				screenOptions={{
					tabBarActiveTintColor: mainColor,
					tabBarInactiveTintColor: '#666666',
					headerStyle: {
						backgroundColor: '#F8F8F8',
						borderBottomWidth: 0,
					},
					headerTitleAlign: 'center',
					tabBarBadgeStyle: {
						fontSize: 8,
						lineHeight: 12,
						height: 12,
						minWidth: 12,
						borderRadius: 6,
					},
				}}>
				<Tab.Screen
					name="首页"
					component={HomeScreen}
					options={{
						tabBarLabel: t('tab.tab_home'),
						tabBarIcon: ({focused, color, size}) => (
							<JFLIcon.tab_home color={focused ? mainColor : '#666666'} />
						),
					}}
				/>
				<Tab.Screen
					name="工单"
					component={OrderScreen}
					options={{
						tabBarBadge: unreadNum1 > 0 ? '' : null,
						tabBarLabel: t('tab.tab_order'),
						tabBarIcon: ({focused, color, size}) => (
							<JFLIcon.tab_order color={focused ? mainColor : '#666666'} />
						),
					}}
				/>
				<Tab.Screen
					name="活动"
					component={ActivityScreen}
					options={{
						tabBarBadge: unreadNum2 > 0 ? '' : null,
						tabBarLabel: t('tab.tab_activity'),
						tabBarIcon: ({focused, color, size}) => (
							<JFLIcon.tab_activity color={focused ? mainColor : '#666666'} />
						),
					}}
				/>
				<Tab.Screen
					name="我的"
					component={MyScreen}
					options={{
						tabBarBadge: unreadNum3 > 0 ? '' : null,
						tabBarLabel: t('tab.tab_my'),
						tabBarIcon: ({focused, color, size}) => (
							<JFLIcon.tab_my color={focused ? mainColor : '#666666'} />
						),
					}}
				/>
			</Tab.Navigator>
		</>
	);
}
