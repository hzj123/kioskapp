import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Box, StatusBar, IconButton, useToast} from 'native-base';
import {Platform, DeviceEventEmitter} from 'react-native';

import {
	storage,
	Tast,
	JFLIcon,
	Loading,
	mainColor,
	pxToDpNum,
	UserNetwork,
} from '../../common/commonIndex.js';

import {LogBox} from 'react-native';

LogBox.ignoreLogs(['Setting a timer']);
LogBox.ignoreLogs(['componentWillReceiveProps has been renamed']);
LogBox.ignoreLogs(['componentWillMount has been renamed']);

import SplashScreen from 'react-native-splash-screen';

import {Login, RegisterPage} from '../login/login.js';
import TabScreen from './tabScreen.js';
import CreateOrder from '../home/createOrder.js';
import HistoryOrderList from '../home/historyOrderList.js';
import QuestionList from '../home/questionList.js';
import QuestionDetail from '../home/questionDetail.js';
import LinkList, {AddLink, WebDetail} from '../home/linkList.js';
import SearchList from '../home/searchList.js';

import OrderDetail from '../order/orderDetail.js';

import Personal, {PersonalEditText} from '../my/personal.js';
import ChangePsw from '../my/changePsw.js';
import ServiceList from '../my/serviceList.js';
import MessageTabList, {MessageSystemDetail} from '../my/messageTabList.js';
import EmployeeList, {EmployeeEdit} from '../my/employeeList.js';

import ActivityDetail from '../activity/activityDetail.js';

global.showLoading = false;

import {useTranslation} from 'react-i18next';

import {useFocusEffect} from '@react-navigation/native';

function StartPage({navigation, route}) {
	const {i18n} = useTranslation();

	/*获取语言*/
	_getLanguage = () => {
		//只返回resolve
		return new Promise((resolve, reject) => {
			storage
				.load({
					key: 'language',
				})
				.then(lan => {
					if (lan != null) {
						i18n.changeLanguage(lan);
					}
					resolve(lan);
				})
				.catch(err => {
					resolve(err);
				});
		});
	};

	/*获取用户信息*/
	_getUserInfo = () => {
		return new Promise((resolve, reject) => {
			storage
				.load({
					key: 'token',
				})
				.then(res => {
					global.token = res;
					return UserNetwork.findUserId();
				})
				.then(res => {
					global.user = res;
					resolve(res);
				})
				.catch(err => {
					reject(err);
				});
		});
	};

	/*启动时的加载方法*/
	_getStartInfo = () => {
		global.showLoading();
		Promise.all([_getUserInfo(), _getLanguage()])
			.then(res => {
				global.hiddenLoading();
				/*跳转到主页面*/
				navigation.navigate('Tab');
			})
			.catch(err => {
				global.hiddenLoading();
				/*跳转到登录页面*/
				navigation.reset({
					index: 0,
					routes: [
						{
							name: 'Login',
						},
					],
				});
			});
	};

	React.useEffect(() => {
		_getStartInfo();
	}, [navigation, route]);

	return (
		<Box flex={1} bg={mainColor}>
			<StatusBar
				backgroundColor={mainColor}
				barStyle="dark-content"
				hidden={true}
			/>
		</Box>
	);
}

import {Text} from 'native-base';

//详情页面
function DetailsScreen1() {
	return (
		<Box style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
			<Text>Details112312</Text>
		</Box>
	);
}

//路由
const RootStack = createNativeStackNavigator();

export default function RootScreen(props) {
	const [showLoading, setShowLoading] = React.useState(false);

	const navigationRef = React.useRef(null);

	const toast = useToast();

	global.showLoading = React.useCallback(() => {
		setShowLoading(true);
		setTimeout(() => {
			if (showLoading) {
				setShowLoading(false);
			}
		}, 100000);
		return () => {
			setShowLoading(false);
		};
	}, [props]);

	global.hiddenLoading = React.useCallback(() => {
		setShowLoading(false);
	}, [props]);

	global.showToast = info => {
		if (info?.title?.length > 0) {
			toast.show({
				title: info?.title ?? '',
				placement: info?.placement ?? 'bottom',
				duration: info?.duration,
			});
		}
	};

	React.useEffect(() => {
		SplashScreen.hide();
	}, [props.reload]);

	global.saveUser = React.useCallback(() => {
		Tast.saveUser(global.user)
			.then(_ => {})
			.catch(_ => {})
			.finally(_ => {});
	}, [global.user]);

	return (
		<NavigationContainer ref={navigationRef}>
			{showLoading && <Loading />}
			<StatusBar backgroundColor="#F8F8F8" barStyle="dark-content" />
			<RootStack.Navigator
				screenOptions={{
					headerStyle: {
						backgroundColor: '#F8F8F8',
						borderBottomWidth: 0,
					},
					cardShadowEnabled: true,
					headerTintColor: '#333',
					headerTitleStyle: {
						fontSize: pxToDpNum(18),
						fontWeight: '500',
						color: '#333333',
					},
					headerMode: 'screen',
					headerTitleAlign: 'center',
					headerBackTitleVisible: false,
					headerLeftContainerStyle: {
						marginLeft: pxToDpNum(10),
					},
					// headerBackImage: props => (
					// 	<Box
					// 		justifyContent="center"
					// 		bg="#010101"
					// 		alignItems={Platform.OS === 'ios' ? 'center' : 'flex-start'}
					// 		size={'40px'}>
					// 		<JFLIcon.login_name />
					// 	</Box>
					// ),
					headerPressColorAndroid: '#F8F8F8',
				}}>
				<RootStack.Group
					screenOptions={{
						headerShown: false,
						gestureEnabled: false,
						animation: 'none',
					}}>
					<RootStack.Screen name="StartPage" component={StartPage} />
				</RootStack.Group>
				<RootStack.Screen
					name="Login"
					component={Login}
					options={{
						headerShown: false,
						gestureEnabled: false,
						animation: 'fade_from_bottom',
					}}
				/>
				<RootStack.Screen
					name="RegisterPage"
					component={RegisterPage}
					options={{
						headerTransparent: true,
						headerStyle: {},
						title: '',
						headerTintColor: '#ffff',
					}}
				/>

				<RootStack.Screen
					name="Tab"
					component={TabScreen}
					options={{headerShown: false, gestureEnabled: false}}
				/>
				<RootStack.Screen name="CreateOrder" component={CreateOrder} />
				<RootStack.Screen
					name="HistoryOrderList"
					component={HistoryOrderList}
				/>
				<RootStack.Screen name="QuestionList" component={QuestionList} />
				<RootStack.Screen name="QuestionDetail" component={QuestionDetail} />
				<RootStack.Screen name="LinkList" component={LinkList} />
				<RootStack.Screen name="AddLink" component={AddLink} />
				<RootStack.Screen name="WebDetail" component={WebDetail} />

				<RootStack.Screen name="SearchList" component={SearchList} />

				<RootStack.Screen name="OrderDetail" component={OrderDetail} />

				<RootStack.Screen name="Personal" component={Personal} />
				<RootStack.Screen
					name="PersonalEditText"
					component={PersonalEditText}
				/>
				<RootStack.Screen name="ChangePsw" component={ChangePsw} />
				<RootStack.Screen name="ServiceList" component={ServiceList} />
				<RootStack.Screen name="MessageTabList" component={MessageTabList} />
				<RootStack.Screen
					name="MessageSystemDetail"
					component={MessageSystemDetail}
				/>
				<RootStack.Screen name="EmployeeList" component={EmployeeList} />
				<RootStack.Screen name="EmployeeEdit" component={EmployeeEdit} />
				<RootStack.Screen name="ActivityDetail" component={ActivityDetail} />

				<RootStack.Screen name="DetailsScreen1" component={DetailsScreen1} />
			</RootStack.Navigator>
		</NavigationContainer>
	);
}
