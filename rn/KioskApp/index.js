/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import './app/common/language/language_config.js';

AppRegistry.registerComponent(appName, () => App);
